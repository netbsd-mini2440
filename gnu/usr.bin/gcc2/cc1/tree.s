#NO_APP
gcc_compiled.:
.globl _standard_tree_code_type
.text
LC0:
	.ascii "x\0"
LC1:
	.ascii "b\0"
LC2:
	.ascii "t\0"
LC3:
	.ascii "c\0"
LC4:
	.ascii "d\0"
LC5:
	.ascii "r\0"
LC6:
	.ascii "e\0"
LC7:
	.ascii "2\0"
LC8:
	.ascii "1\0"
LC9:
	.ascii "<\0"
LC10:
	.ascii "s\0"
.data
	.align 2
_standard_tree_code_type:
	.long LC0
	.long LC0
	.long LC0
	.long LC0
	.long LC0
	.long LC1
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC2
	.long LC3
	.long LC3
	.long LC3
	.long LC3
	.long LC4
	.long LC4
	.long LC4
	.long LC4
	.long LC4
	.long LC4
	.long LC4
	.long LC4
	.long LC5
	.long LC5
	.long LC5
	.long LC5
	.long LC5
	.long LC5
	.long LC6
	.long LC6
	.long LC6
	.long LC6
	.long LC6
	.long LC6
	.long LC6
	.long LC6
	.long LC6
	.long LC6
	.long LC7
	.long LC7
	.long LC7
	.long LC7
	.long LC7
	.long LC7
	.long LC7
	.long LC7
	.long LC7
	.long LC7
	.long LC7
	.long LC7
	.long LC7
	.long LC8
	.long LC8
	.long LC8
	.long LC8
	.long LC8
	.long LC7
	.long LC8
	.long LC7
	.long LC7
	.long LC8
	.long LC8
	.long LC7
	.long LC7
	.long LC7
	.long LC7
	.long LC7
	.long LC7
	.long LC7
	.long LC7
	.long LC8
	.long LC6
	.long LC6
	.long LC7
	.long LC7
	.long LC7
	.long LC6
	.long LC9
	.long LC9
	.long LC9
	.long LC9
	.long LC9
	.long LC9
	.long LC7
	.long LC9
	.long LC8
	.long LC7
	.long LC8
	.long LC8
	.long LC8
	.long LC6
	.long LC6
	.long LC6
	.long LC6
	.long LC6
	.long LC7
	.long LC8
	.long LC8
	.long LC8
	.long LC6
	.long LC6
	.long LC6
	.long LC6
	.long LC10
	.long LC10
	.long LC10
	.long LC10
	.long LC10
.globl _standard_tree_code_length
	.align 2
_standard_tree_code_length:
	.long 0
	.long -1
	.long 2
	.long 2
	.long 2
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 2
	.long 3
	.long 3
	.long 3
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 0
	.long 2
	.long 3
	.long 1
	.long 2
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 3
	.long 3
	.long 3
	.long 3
	.long 4
	.long 3
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
	.long 2
	.long 1
	.long 2
	.long 2
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 2
	.long 1
	.long 2
	.long 1
	.long 1
	.long 1
	.long 3
	.long 2
	.long 1
	.long 1
	.long 1
	.long 2
	.long 1
	.long 1
	.long 1
	.long 2
	.long 2
	.long 2
	.long 2
	.long 1
	.long 1
	.long 1
	.long 1
	.long 1
.globl _standard_tree_code_name
.text
LC11:
	.ascii "error_mark\0"
LC12:
	.ascii "identifier_node\0"
LC13:
	.ascii "op_identifier\0"
LC14:
	.ascii "tree_list\0"
LC15:
	.ascii "tree_vec\0"
LC16:
	.ascii "block\0"
LC17:
	.ascii "void_type\0"
LC18:
	.ascii "integer_type\0"
LC19:
	.ascii "real_type\0"
LC20:
	.ascii "complex_type\0"
LC21:
	.ascii "enumeral_type\0"
LC22:
	.ascii "boolean_type\0"
LC23:
	.ascii "char_type\0"
LC24:
	.ascii "pointer_type\0"
LC25:
	.ascii "offset_type\0"
LC26:
	.ascii "reference_type\0"
LC27:
	.ascii "method_type\0"
LC28:
	.ascii "file_type\0"
LC29:
	.ascii "array_type\0"
LC30:
	.ascii "set_type\0"
LC31:
	.ascii "string_type\0"
LC32:
	.ascii "record_type\0"
LC33:
	.ascii "union_type\0"
LC34:
	.ascii "qual_union_type\0"
LC35:
	.ascii "function_type\0"
LC36:
	.ascii "lang_type\0"
LC37:
	.ascii "integer_cst\0"
LC38:
	.ascii "real_cst\0"
LC39:
	.ascii "complex_cst\0"
LC40:
	.ascii "string_cst\0"
LC41:
	.ascii "function_decl\0"
LC42:
	.ascii "label_decl\0"
LC43:
	.ascii "const_decl\0"
LC44:
	.ascii "type_decl\0"
LC45:
	.ascii "var_decl\0"
LC46:
	.ascii "parm_decl\0"
LC47:
	.ascii "result_decl\0"
LC48:
	.ascii "field_decl\0"
LC49:
	.ascii "component_ref\0"
LC50:
	.ascii "bit_field_ref\0"
LC51:
	.ascii "indirect_ref\0"
LC52:
	.ascii "offset_ref\0"
LC53:
	.ascii "buffer_ref\0"
LC54:
	.ascii "array_ref\0"
LC55:
	.ascii "constructor\0"
LC56:
	.ascii "compound_expr\0"
LC57:
	.ascii "modify_expr\0"
LC58:
	.ascii "init_expr\0"
LC59:
	.ascii "target_expr\0"
LC60:
	.ascii "cond_expr\0"
LC61:
	.ascii "bind_expr\0"
LC62:
	.ascii "call_expr\0"
LC63:
	.ascii "method_call_expr\0"
LC64:
	.ascii "with_cleanup_expr\0"
LC65:
	.ascii "plus_expr\0"
LC66:
	.ascii "minus_expr\0"
LC67:
	.ascii "mult_expr\0"
LC68:
	.ascii "trunc_div_expr\0"
LC69:
	.ascii "ceil_div_expr\0"
LC70:
	.ascii "floor_div_expr\0"
LC71:
	.ascii "round_div_expr\0"
LC72:
	.ascii "trunc_mod_expr\0"
LC73:
	.ascii "ceil_mod_expr\0"
LC74:
	.ascii "floor_mod_expr\0"
LC75:
	.ascii "round_mod_expr\0"
LC76:
	.ascii "rdiv_expr\0"
LC77:
	.ascii "exact_div_expr\0"
LC78:
	.ascii "fix_trunc_expr\0"
LC79:
	.ascii "fix_ceil_expr\0"
LC80:
	.ascii "fix_floor_expr\0"
LC81:
	.ascii "fix_round_expr\0"
LC82:
	.ascii "float_expr\0"
LC83:
	.ascii "expon_expr\0"
LC84:
	.ascii "negate_expr\0"
LC85:
	.ascii "min_expr\0"
LC86:
	.ascii "max_expr\0"
LC87:
	.ascii "abs_expr\0"
LC88:
	.ascii "ffs_expr\0"
LC89:
	.ascii "alshift_expr\0"
LC90:
	.ascii "arshift_expr\0"
LC91:
	.ascii "lrotate_expr\0"
LC92:
	.ascii "rrotate_expr\0"
LC93:
	.ascii "bit_ior_expr\0"
LC94:
	.ascii "bit_xor_expr\0"
LC95:
	.ascii "bit_and_expr\0"
LC96:
	.ascii "bit_andtc_expr\0"
LC97:
	.ascii "bit_not_expr\0"
LC98:
	.ascii "truth_andif_expr\0"
LC99:
	.ascii "truth_orif_expr\0"
LC100:
	.ascii "truth_and_expr\0"
LC101:
	.ascii "truth_or_expr\0"
LC102:
	.ascii "truth_xor_expr\0"
LC103:
	.ascii "truth_not_expr\0"
LC104:
	.ascii "lt_expr\0"
LC105:
	.ascii "le_expr\0"
LC106:
	.ascii "gt_expr\0"
LC107:
	.ascii "ge_expr\0"
LC108:
	.ascii "eq_expr\0"
LC109:
	.ascii "ne_expr\0"
LC110:
	.ascii "in_expr\0"
LC111:
	.ascii "set_le_expr\0"
LC112:
	.ascii "card_expr\0"
LC113:
	.ascii "range_expr\0"
LC114:
	.ascii "convert_expr\0"
LC115:
	.ascii "nop_expr\0"
LC116:
	.ascii "non_lvalue_expr\0"
LC117:
	.ascii "save_expr\0"
LC118:
	.ascii "rtl_expr\0"
LC119:
	.ascii "addr_expr\0"
LC120:
	.ascii "reference_expr\0"
LC121:
	.ascii "entry_value_expr\0"
LC122:
	.ascii "complex_expr\0"
LC123:
	.ascii "conj_expr\0"
LC124:
	.ascii "realpart_expr\0"
LC125:
	.ascii "imagpart_expr\0"
LC126:
	.ascii "predecrement_expr\0"
LC127:
	.ascii "preincrement_expr\0"
LC128:
	.ascii "postdecrement_expr\0"
LC129:
	.ascii "postincrement_expr\0"
LC130:
	.ascii "label_expr\0"
LC131:
	.ascii "goto_expr\0"
LC132:
	.ascii "return_expr\0"
LC133:
	.ascii "exit_expr\0"
LC134:
	.ascii "loop_expr\0"
.data
	.align 2
_standard_tree_code_name:
	.long LC11
	.long LC12
	.long LC13
	.long LC14
	.long LC15
	.long LC16
	.long LC17
	.long LC18
	.long LC19
	.long LC20
	.long LC21
	.long LC22
	.long LC23
	.long LC24
	.long LC25
	.long LC26
	.long LC27
	.long LC28
	.long LC29
	.long LC30
	.long LC31
	.long LC32
	.long LC33
	.long LC34
	.long LC35
	.long LC36
	.long LC37
	.long LC38
	.long LC39
	.long LC40
	.long LC41
	.long LC42
	.long LC43
	.long LC44
	.long LC45
	.long LC46
	.long LC47
	.long LC48
	.long LC49
	.long LC50
	.long LC51
	.long LC52
	.long LC53
	.long LC54
	.long LC55
	.long LC56
	.long LC57
	.long LC58
	.long LC59
	.long LC60
	.long LC61
	.long LC62
	.long LC63
	.long LC64
	.long LC65
	.long LC66
	.long LC67
	.long LC68
	.long LC69
	.long LC70
	.long LC71
	.long LC72
	.long LC73
	.long LC74
	.long LC75
	.long LC76
	.long LC77
	.long LC78
	.long LC79
	.long LC80
	.long LC81
	.long LC82
	.long LC83
	.long LC84
	.long LC85
	.long LC86
	.long LC87
	.long LC88
	.long LC89
	.long LC90
	.long LC91
	.long LC92
	.long LC93
	.long LC94
	.long LC95
	.long LC96
	.long LC97
	.long LC98
	.long LC99
	.long LC100
	.long LC101
	.long LC102
	.long LC103
	.long LC104
	.long LC105
	.long LC106
	.long LC107
	.long LC108
	.long LC109
	.long LC110
	.long LC111
	.long LC112
	.long LC113
	.long LC114
	.long LC115
	.long LC116
	.long LC117
	.long LC118
	.long LC119
	.long LC120
	.long LC121
	.long LC122
	.long LC123
	.long LC124
	.long LC125
	.long LC126
	.long LC127
	.long LC128
	.long LC129
	.long LC130
	.long LC131
	.long LC132
	.long LC133
	.long LC134
.globl _id_string_size
	.align 2
_id_string_size:
	.long 0
.globl _tree_node_kind_names
.text
LC135:
	.ascii "decls\0"
LC136:
	.ascii "types\0"
LC137:
	.ascii "blocks\0"
LC138:
	.ascii "stmts\0"
LC139:
	.ascii "refs\0"
LC140:
	.ascii "exprs\0"
LC141:
	.ascii "constants\0"
LC142:
	.ascii "identifiers\0"
LC143:
	.ascii "op_identifiers\0"
LC144:
	.ascii "perm_tree_lists\0"
LC145:
	.ascii "temp_tree_lists\0"
LC146:
	.ascii "vecs\0"
LC147:
	.ascii "random kinds\0"
LC148:
	.ascii "lang_decl kinds\0"
LC149:
	.ascii "lang_type kinds\0"
.data
	.align 2
_tree_node_kind_names:
	.long LC135
	.long LC136
	.long LC137
	.long LC138
	.long LC139
	.long LC140
	.long LC141
	.long LC142
	.long LC143
	.long LC144
	.long LC145
	.long LC146
	.long LC147
	.long LC148
	.long LC149
	.align 2
_next_type_uid:
	.long 1
.text
	.align 2
.globl _init_obstacks
_init_obstacks:
	pushl %ebp
	movl %esp,%ebp
	subl $80,%esp
	pushl %ebx
	pushl $_obstack_stack_obstack
	call _gcc_obstack_init
	pushl $_permanent_obstack
	call _gcc_obstack_init
	pushl $_temporary_obstack
	call _gcc_obstack_init
	movl $_temporary_obstack,-4(%ebp)
	movl -4(%ebp),%ebx
	movl %ebx,-8(%ebp)
	movl $0,-12(%ebp)
	movl -8(%ebp),%eax
	movl -8(%ebp),%edx
	movl 16(%eax),%eax
	subl 12(%edx),%eax
	cmpl -12(%ebp),%eax
	jge L10
	pushl -12(%ebp)
	pushl -8(%ebp)
	call __obstack_newchunk
	addl $8,%esp
	xorl %eax,%eax
	jmp L11
L10:
	xorl %eax,%eax
L11:
	movl -8(%ebp),%eax
	movl %eax,%eax
	movl 12(%eax),%edx
	addl -12(%ebp),%edx
	movl %edx,%edx
	movl %edx,12(%eax)
	movl -4(%ebp),%ebx
	movl %ebx,-16(%ebp)
	movl -16(%ebp),%eax
	movl 8(%eax),%ebx
	movl %ebx,-20(%ebp)
	movl -16(%ebp),%eax
	cmpl 12(%eax),%ebx
	jne L12
	movl -16(%ebp),%eax
	movl %eax,%eax
	orb $2,40(%eax)
	movb 40(%eax),%al
	shrb $1,%al
	movzbl %al,%eax
	andl $1,%eax
L12:
	movl -16(%ebp),%eax
	movl -16(%ebp),%edx
	movl -16(%ebp),%ecx
	movl 12(%edx),%edx
	addl 24(%ecx),%edx
	movl -16(%ebp),%ecx
	movl 24(%ecx),%ecx
	notl %ecx
	andl %ecx,%edx
	movl %edx,%edx
	movl %edx,12(%eax)
	movl -16(%ebp),%eax
	movl -16(%ebp),%edx
	movl 12(%eax),%eax
	subl 4(%edx),%eax
	movl -16(%ebp),%edx
	movl -16(%ebp),%ecx
	movl 16(%edx),%edx
	subl 4(%ecx),%edx
	cmpl %eax,%edx
	jge L13
	movl -16(%ebp),%eax
	movl -16(%ebp),%edx
	movl 16(%edx),%edx
	movl %edx,12(%eax)
	movl %edx,%eax
	jmp L14
L13:
	xorl %eax,%eax
L14:
	movl -16(%ebp),%eax
	movl -16(%ebp),%edx
	movl 12(%edx),%edx
	movl %edx,8(%eax)
	movl -20(%ebp),%ebx
	movl %ebx,_temporary_firstobj
	pushl $_momentary_obstack
	call _gcc_obstack_init
	movl $_momentary_obstack,-24(%ebp)
	movl -24(%ebp),%ebx
	movl %ebx,-28(%ebp)
	movl $0,-32(%ebp)
	movl -28(%ebp),%eax
	movl -28(%ebp),%edx
	movl 16(%eax),%eax
	subl 12(%edx),%eax
	cmpl -32(%ebp),%eax
	jge L15
	pushl -32(%ebp)
	pushl -28(%ebp)
	call __obstack_newchunk
	addl $8,%esp
	xorl %eax,%eax
	jmp L16
L15:
	xorl %eax,%eax
L16:
	movl -28(%ebp),%eax
	movl %eax,%eax
	movl 12(%eax),%edx
	addl -32(%ebp),%edx
	movl %edx,%edx
	movl %edx,12(%eax)
	movl -24(%ebp),%ebx
	movl %ebx,-36(%ebp)
	movl -36(%ebp),%eax
	movl 8(%eax),%ebx
	movl %ebx,-40(%ebp)
	movl -36(%ebp),%eax
	cmpl 12(%eax),%ebx
	jne L17
	movl -36(%ebp),%eax
	movl %eax,%eax
	orb $2,40(%eax)
	movb 40(%eax),%al
	shrb $1,%al
	movzbl %al,%eax
	andl $1,%eax
L17:
	movl -36(%ebp),%eax
	movl -36(%ebp),%edx
	movl -36(%ebp),%ecx
	movl 12(%edx),%edx
	addl 24(%ecx),%edx
	movl -36(%ebp),%ecx
	movl 24(%ecx),%ecx
	notl %ecx
	andl %ecx,%edx
	movl %edx,%edx
	movl %edx,12(%eax)
	movl -36(%ebp),%eax
	movl -36(%ebp),%edx
	movl 12(%eax),%eax
	subl 4(%edx),%eax
	movl -36(%ebp),%edx
	movl -36(%ebp),%ecx
	movl 16(%edx),%edx
	subl 4(%ecx),%edx
	cmpl %eax,%edx
	jge L18
	movl -36(%ebp),%eax
	movl -36(%ebp),%edx
	movl 16(%edx),%edx
	movl %edx,12(%eax)
	movl %edx,%eax
	jmp L19
L18:
	xorl %eax,%eax
L19:
	movl -36(%ebp),%eax
	movl -36(%ebp),%edx
	movl 12(%edx),%edx
	movl %edx,8(%eax)
	movl -40(%ebp),%ebx
	movl %ebx,_momentary_firstobj
	pushl $_maybepermanent_obstack
	call _gcc_obstack_init
	movl $_maybepermanent_obstack,-44(%ebp)
	movl -44(%ebp),%ebx
	movl %ebx,-48(%ebp)
	movl $0,-52(%ebp)
	movl -48(%ebp),%eax
	movl -48(%ebp),%edx
	movl 16(%eax),%eax
	subl 12(%edx),%eax
	cmpl -52(%ebp),%eax
	jge L20
	pushl -52(%ebp)
	pushl -48(%ebp)
	call __obstack_newchunk
	addl $8,%esp
	xorl %eax,%eax
	jmp L21
L20:
	xorl %eax,%eax
L21:
	movl -48(%ebp),%eax
	movl %eax,%eax
	movl 12(%eax),%edx
	addl -52(%ebp),%edx
	movl %edx,%edx
	movl %edx,12(%eax)
	movl -44(%ebp),%ebx
	movl %ebx,-56(%ebp)
	movl -56(%ebp),%eax
	movl 8(%eax),%ebx
	movl %ebx,-60(%ebp)
	movl -56(%ebp),%eax
	cmpl 12(%eax),%ebx
	jne L22
	movl -56(%ebp),%eax
	movl %eax,%eax
	orb $2,40(%eax)
	movb 40(%eax),%al
	shrb $1,%al
	movzbl %al,%eax
	andl $1,%eax
L22:
	movl -56(%ebp),%eax
	movl -56(%ebp),%edx
	movl -56(%ebp),%ecx
	movl 12(%edx),%edx
	addl 24(%ecx),%edx
	movl -56(%ebp),%ecx
	movl 24(%ecx),%ecx
	notl %ecx
	andl %ecx,%edx
	movl %edx,%edx
	movl %edx,12(%eax)
	movl -56(%ebp),%eax
	movl -56(%ebp),%edx
	movl 12(%eax),%eax
	subl 4(%edx),%eax
	movl -56(%ebp),%edx
	movl -56(%ebp),%ecx
	movl 16(%edx),%edx
	subl 4(%ecx),%edx
	cmpl %eax,%edx
	jge L23
	movl -56(%ebp),%eax
	movl -56(%ebp),%edx
	movl 16(%edx),%edx
	movl %edx,12(%eax)
	movl %edx,%eax
	jmp L24
L23:
	xorl %eax,%eax
L24:
	movl -56(%ebp),%eax
	movl -56(%ebp),%edx
	movl 12(%edx),%edx
	movl %edx,8(%eax)
	movl -60(%ebp),%ebx
	movl %ebx,_maybepermanent_firstobj
	pushl $_temp_decl_obstack
	call _gcc_obstack_init
	movl $_temp_decl_obstack,-64(%ebp)
	movl -64(%ebp),%ebx
	movl %ebx,-68(%ebp)
	movl $0,-72(%ebp)
	movl -68(%ebp),%eax
	movl -68(%ebp),%edx
	movl 16(%eax),%eax
	subl 12(%edx),%eax
	cmpl -72(%ebp),%eax
	jge L25
	pushl -72(%ebp)
	pushl -68(%ebp)
	call __obstack_newchunk
	addl $8,%esp
	xorl %eax,%eax
	jmp L26
L25:
	xorl %eax,%eax
L26:
	movl -68(%ebp),%eax
	movl %eax,%eax
	movl 12(%eax),%edx
	addl -72(%ebp),%edx
	movl %edx,%edx
	movl %edx,12(%eax)
	movl -64(%ebp),%ebx
	movl %ebx,-76(%ebp)
	movl -76(%ebp),%eax
	movl 8(%eax),%ebx
	movl %ebx,-80(%ebp)
	movl -76(%ebp),%eax
	cmpl 12(%eax),%ebx
	jne L27
	movl -76(%ebp),%eax
	movl %eax,%eax
	orb $2,40(%eax)
	movb 40(%eax),%al
	shrb $1,%al
	movzbl %al,%eax
	andl $1,%eax
L27:
	movl -76(%ebp),%eax
	movl -76(%ebp),%edx
	movl -76(%ebp),%ecx
	movl 12(%edx),%edx
	addl 24(%ecx),%edx
	movl -76(%ebp),%ecx
	movl 24(%ecx),%ecx
	notl %ecx
	andl %ecx,%edx
	movl %edx,%edx
	movl %edx,12(%eax)
	movl -76(%ebp),%eax
	movl -76(%ebp),%edx
	movl 12(%eax),%eax
	subl 4(%edx),%eax
	movl -76(%ebp),%edx
	movl -76(%ebp),%ecx
	movl 16(%edx),%edx
	subl 4(%ecx),%edx
	cmpl %eax,%edx
	jge L28
	movl -76(%ebp),%eax
	movl -76(%ebp),%edx
	movl 16(%edx),%edx
	movl %edx,12(%eax)
	movl %edx,%eax
	jmp L29
L28:
	xorl %eax,%eax
L29:
	movl -76(%ebp),%eax
	movl -76(%ebp),%edx
	movl 12(%edx),%edx
	movl %edx,8(%eax)
	movl -80(%ebp),%ebx
	movl %ebx,_temp_decl_firstobj
	movl $_temporary_obstack,_function_obstack
	movl $_maybepermanent_obstack,_function_maybepermanent_obstack
	movl $_permanent_obstack,_current_obstack
	movl $_permanent_obstack,_expression_obstack
	movl $_permanent_obstack,%eax
	movl %eax,_saveable_obstack
	movl %eax,_rtl_obstack
	pushl $4036
	pushl $_hash_table
	call _bzero
L9:
	leal -84(%ebp),%esp
	popl %ebx
	leave
	ret
	.align 2
.globl _gcc_obstack_init
_gcc_obstack_init:
	pushl %ebp
	movl %esp,%ebp
	pushl $_free
	pushl $_xmalloc
	pushl $0
	pushl $0
	pushl 8(%ebp)
	call __obstack_begin
L30:
	leave
	ret
	.align 2
.globl _save_tree_status
_save_tree_status:
	pushl %ebp
	movl %esp,%ebp
	subl $16,%esp
	pushl %ebx
	movl 8(%ebp),%eax
	movl _all_types_permanent,%ebx
	movl %ebx,304(%eax)
	movl 8(%ebp),%eax
	movl _momentary_stack,%ebx
	movl %ebx,308(%eax)
	movl 8(%ebp),%eax
	movl _maybepermanent_firstobj,%ebx
	movl %ebx,312(%eax)
	movl 8(%ebp),%eax
	movl _momentary_firstobj,%ebx
	movl %ebx,320(%eax)
	movl 8(%ebp),%eax
	movl _function_obstack,%ebx
	movl %ebx,328(%eax)
	movl 8(%ebp),%eax
	movl _function_maybepermanent_obstack,%ebx
	movl %ebx,332(%eax)
	movl 8(%ebp),%eax
	movl _current_obstack,%ebx
	movl %ebx,324(%eax)
	movl 8(%ebp),%eax
	movl _expression_obstack,%ebx
	movl %ebx,336(%eax)
	movl 8(%ebp),%eax
	movl _saveable_obstack,%ebx
	movl %ebx,340(%eax)
	movl 8(%ebp),%eax
	movl _rtl_obstack,%ebx
	movl %ebx,344(%eax)
	pushl $44
	call _xmalloc
	movl %eax,_function_obstack
	pushl _function_obstack
	call _gcc_obstack_init
	pushl $44
	call _xmalloc
	movl %eax,_function_maybepermanent_obstack
	pushl _function_maybepermanent_obstack
	call _gcc_obstack_init
	movl $_permanent_obstack,_current_obstack
	movl $_permanent_obstack,_expression_obstack
	movl $_permanent_obstack,%eax
	movl %eax,_saveable_obstack
	movl %eax,_rtl_obstack
	movl $_momentary_obstack,-4(%ebp)
	movl -4(%ebp),%eax
	movl 8(%eax),%ebx
	movl %ebx,-8(%ebp)
	movl -4(%ebp),%eax
	cmpl 12(%eax),%ebx
	jne L32
	movl -4(%ebp),%eax
	movl %eax,%eax
	orb $2,40(%eax)
	movb 40(%eax),%al
	shrb $1,%al
	movzbl %al,%eax
	andl $1,%eax
L32:
	movl -4(%ebp),%eax
	movl -4(%ebp),%edx
	movl -4(%ebp),%ecx
	movl 12(%edx),%edx
	addl 24(%ecx),%edx
	movl -4(%ebp),%ecx
	movl 24(%ecx),%ecx
	notl %ecx
	andl %ecx,%edx
	movl %edx,%edx
	movl %edx,12(%eax)
	movl -4(%ebp),%eax
	movl -4(%ebp),%edx
	movl 12(%eax),%eax
	subl 4(%edx),%eax
	movl -4(%ebp),%edx
	movl -4(%ebp),%ecx
	movl 16(%edx),%edx
	subl 4(%ecx),%edx
	cmpl %eax,%edx
	jge L33
	movl -4(%ebp),%eax
	movl -4(%ebp),%edx
	movl 16(%edx),%edx
	movl %edx,12(%eax)
	movl %edx,%eax
	jmp L34
L33:
	xorl %eax,%eax
L34:
	movl -4(%ebp),%eax
	movl -4(%ebp),%edx
	movl 12(%edx),%edx
	movl %edx,8(%eax)
	movl -8(%ebp),%ebx
	movl %ebx,_momentary_firstobj
	movl _function_maybepermanent_obstack,%ebx
	movl %ebx,-12(%ebp)
	movl -12(%ebp),%eax
	movl 8(%eax),%ebx
	movl %ebx,-16(%ebp)
	movl -12(%ebp),%eax
	cmpl 12(%eax),%ebx
	jne L35
	movl -12(%ebp),%eax
	movl %eax,%eax
	orb $2,40(%eax)
	movb 40(%eax),%al
	shrb $1,%al
	movzbl %al,%eax
	andl $1,%eax
L35:
	movl -12(%ebp),%eax
	movl -12(%ebp),%edx
	movl -12(%ebp),%ecx
	movl 12(%edx),%edx
	addl 24(%ecx),%edx
	movl -12(%ebp),%ecx
	movl 24(%ecx),%ecx
	notl %ecx
	andl %ecx,%edx
	movl %edx,%edx
	movl %edx,12(%eax)
	movl -12(%ebp),%eax
	movl -12(%ebp),%edx
	movl 12(%eax),%eax
	subl 4(%edx),%eax
	movl -12(%ebp),%edx
	movl -12(%ebp),%ecx
	movl 16(%edx),%edx
	subl 4(%ecx),%edx
	cmpl %eax,%edx
	jge L36
	movl -12(%ebp),%eax
	movl -12(%ebp),%edx
	movl 16(%edx),%edx
	movl %edx,12(%eax)
	movl %edx,%eax
	jmp L37
L36:
	xorl %eax,%eax
L37:
	movl -12(%ebp),%eax
	movl -12(%ebp),%edx
	movl 12(%edx),%edx
	movl %edx,8(%eax)
	movl -16(%ebp),%ebx
	movl %ebx,_maybepermanent_firstobj
L31:
	leal -20(%ebp),%esp
	popl %ebx
	leave
	ret
	.align 2
.globl _restore_tree_status
_restore_tree_status:
	pushl %ebp
	movl %esp,%ebp
	subl $24,%esp
	pushl %ebx
	movl 8(%ebp),%eax
	movl 304(%eax),%ebx
	movl %ebx,_all_types_permanent
	movl 8(%ebp),%eax
	movl 308(%eax),%ebx
	movl %ebx,_momentary_stack
	movl $_momentary_obstack,-4(%ebp)
	movl _momentary_firstobj,%ebx
	movl %ebx,-8(%ebp)
	movl -4(%ebp),%eax
	movl 4(%eax),%ebx
	cmpl -8(%ebp),%ebx
	jae L39
	movl -4(%ebp),%eax
	movl 16(%eax),%ebx
	cmpl -8(%ebp),%ebx
	jbe L39
	movl -4(%ebp),%eax
	movl -4(%ebp),%edx
	movl -8(%ebp),%ecx
	movl %ecx,8(%edx)
	movl %ecx,%edx
	movl %edx,12(%eax)
	jmp L40
L39:
	pushl -8(%ebp)
	pushl -4(%ebp)
	call _obstack_free
	addl $8,%esp
L40:
	movl _function_obstack,%ebx
	movl %ebx,-12(%ebp)
	movl $0,-16(%ebp)
	movl -12(%ebp),%eax
	movl 4(%eax),%ebx
	cmpl -16(%ebp),%ebx
	jae L41
	movl -12(%ebp),%eax
	movl 16(%eax),%ebx
	cmpl -16(%ebp),%ebx
	jbe L41
	movl -12(%ebp),%eax
	movl -12(%ebp),%edx
	movl -16(%ebp),%ecx
	movl %ecx,8(%edx)
	movl %ecx,%edx
	movl %edx,12(%eax)
	jmp L42
L41:
	pushl -16(%ebp)
	pushl -12(%ebp)
	call _obstack_free
	addl $8,%esp
L42:
	movl _function_maybepermanent_obstack,%ebx
	movl %ebx,-20(%ebp)
	movl $0,-24(%ebp)
	movl -20(%ebp),%eax
	movl 4(%eax),%ebx
	cmpl -24(%ebp),%ebx
	jae L43
	movl -20(%ebp),%eax
	movl 16(%eax),%ebx
	cmpl -24(%ebp),%ebx
	jbe L43
	movl -20(%ebp),%eax
	movl -20(%ebp),%edx
	movl -24(%ebp),%ecx
	movl %ecx,8(%edx)
	movl %ecx,%edx
	movl %edx,12(%eax)
	jmp L44
L43:
	pushl -24(%ebp)
	pushl -20(%ebp)
	call _obstack_free
	addl $8,%esp
L44:
	pushl _function_obstack
	call _free
	movl 8(%ebp),%eax
	movl 320(%eax),%ebx
	movl %ebx,_momentary_firstobj
	movl 8(%ebp),%eax
	movl 312(%eax),%ebx
	movl %ebx,_maybepermanent_firstobj
	movl 8(%ebp),%eax
	movl 328(%eax),%ebx
	movl %ebx,_function_obstack
	movl 8(%ebp),%eax
	movl 332(%eax),%ebx
	movl %ebx,_function_maybepermanent_obstack
	movl 8(%ebp),%eax
	movl 324(%eax),%ebx
	movl %ebx,_current_obstack
	movl 8(%ebp),%eax
	movl 336(%eax),%ebx
	movl %ebx,_expression_obstack
	movl 8(%ebp),%eax
	movl 340(%eax),%ebx
	movl %ebx,_saveable_obstack
	movl 8(%ebp),%eax
	movl 344(%eax),%ebx
	movl %ebx,_rtl_obstack
L38:
	leal -28(%ebp),%esp
	popl %ebx
	leave
	ret
	.align 2
.globl _temporary_allocation
_temporary_allocation:
	pushl %ebp
	movl %esp,%ebp
	movl _function_obstack,%edx
	movl %edx,_current_obstack
	movl _function_obstack,%edx
	movl %edx,_expression_obstack
	movl _function_maybepermanent_obstack,%eax
	movl %eax,_saveable_obstack
	movl %eax,_rtl_obstack
	movl $0,_momentary_stack
L45:
	leave
	ret
	.align 2
.globl _end_temporary_allocation
_end_temporary_allocation:
	pushl %ebp
	movl %esp,%ebp
	movl $_permanent_obstack,_current_obstack
	movl $_permanent_obstack,_expression_obstack
	movl $_permanent_obstack,%eax
	movl %eax,_saveable_obstack
	movl %eax,_rtl_obstack
L46:
	leave
	ret
	.align 2
.globl _resume_temporary_allocation
_resume_temporary_allocation:
	pushl %ebp
	movl %esp,%ebp
	movl _function_obstack,%edx
	movl %edx,_current_obstack
	movl _function_obstack,%edx
	movl %edx,_expression_obstack
	movl _function_maybepermanent_obstack,%eax
	movl %eax,_saveable_obstack
	movl %eax,_rtl_obstack
L47:
	leave
	ret
	.align 2
.globl _saveable_allocation
_saveable_allocation:
	pushl %ebp
	movl %esp,%ebp
	movl _saveable_obstack,%eax
	movl %eax,_current_obstack
	movl %eax,_expression_obstack
L48:
	leave
	ret
	.align 2
.globl _push_obstacks
_push_obstacks:
	pushl %ebp
	movl %esp,%ebp
	subl $24,%esp
	pushl %ebx
	movl $_obstack_stack_obstack,-8(%ebp)
	movl -8(%ebp),%ebx
	movl %ebx,-12(%ebp)
	movl $20,-16(%ebp)
	movl -12(%ebp),%eax
	movl -12(%ebp),%edx
	movl 16(%eax),%eax
	subl 12(%edx),%eax
	cmpl -16(%ebp),%eax
	jge L50
	pushl -16(%ebp)
	pushl -12(%ebp)
	call __obstack_newchunk
	addl $8,%esp
	xorl %eax,%eax
	jmp L51
L50:
	xorl %eax,%eax
L51:
	movl -12(%ebp),%eax
	movl %eax,%eax
	movl 12(%eax),%edx
	addl -16(%ebp),%edx
	movl %edx,%edx
	movl %edx,12(%eax)
	movl -8(%ebp),%ebx
	movl %ebx,-20(%ebp)
	movl -20(%ebp),%eax
	movl 8(%eax),%ebx
	movl %ebx,-24(%ebp)
	movl -20(%ebp),%eax
	cmpl 12(%eax),%ebx
	jne L52
	movl -20(%ebp),%eax
	movl %eax,%eax
	orb $2,40(%eax)
	movb 40(%eax),%al
	shrb $1,%al
	movzbl %al,%eax
	andl $1,%eax
L52:
	movl -20(%ebp),%eax
	movl -20(%ebp),%edx
	movl -20(%ebp),%ecx
	movl 12(%edx),%edx
	addl 24(%ecx),%edx
	movl -20(%ebp),%ecx
	movl 24(%ecx),%ecx
	notl %ecx
	andl %ecx,%edx
	movl %edx,%edx
	movl %edx,12(%eax)
	movl -20(%ebp),%eax
	movl -20(%ebp),%edx
	movl 12(%eax),%eax
	subl 4(%edx),%eax
	movl -20(%ebp),%edx
	movl -20(%ebp),%ecx
	movl 16(%edx),%edx
	subl 4(%ecx),%edx
	cmpl %eax,%edx
	jge L53
	movl -20(%ebp),%eax
	movl -20(%ebp),%edx
	movl 16(%edx),%edx
	movl %edx,12(%eax)
	movl %edx,%eax
	jmp L54
L53:
	xorl %eax,%eax
L54:
	movl -20(%ebp),%eax
	movl -20(%ebp),%edx
	movl 12(%edx),%edx
	movl %edx,8(%eax)
	movl -24(%ebp),%ebx
	movl %ebx,-4(%ebp)
	movl -4(%ebp),%eax
	movl _current_obstack,%ebx
	movl %ebx,4(%eax)
	movl -4(%ebp),%eax
	movl _saveable_obstack,%ebx
	movl %ebx,8(%eax)
	movl -4(%ebp),%eax
	movl _expression_obstack,%ebx
	movl %ebx,12(%eax)
	movl -4(%ebp),%eax
	movl _rtl_obstack,%ebx
	movl %ebx,16(%eax)
	movl -4(%ebp),%eax
	movl _obstack_stack,%ebx
	movl %ebx,(%eax)
	movl -4(%ebp),%ebx
	movl %ebx,_obstack_stack
	movl 8(%ebp),%ebx
	movl %ebx,_current_obstack
	movl 8(%ebp),%ebx
	movl %ebx,_expression_obstack
	movl 12(%ebp),%eax
	movl %eax,_saveable_obstack
	movl %eax,_rtl_obstack
L49:
	leal -28(%ebp),%esp
	popl %ebx
	leave
	ret
