/*	$NetBSD: rijndael_local.h,v 1.2.28.4 2005/03/04 16:40:52 skrll Exp $	*/
/*	$KAME: rijndael_local.h,v 1.4 2003/07/15 10:47:16 itojun Exp $	*/

/* the file should not be used from outside */
typedef u_int8_t		u8;
typedef u_int16_t		u16;
typedef u_int32_t		u32;
