/*	$NetBSD: r128fb.c,v 1.8 2009/05/06 18:41:54 elad Exp $	*/

/*
 * Copyright (c) 2009 Michael Lorenz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Silicon Motion SM502 / Voyager GX register definitions */

#ifndef SM502REG_H
#define SM502REG_H

/* System Control Registers */
#define SM502_SYSTEM_CTRL	0x00000000
#define 	SM502_SYSCTL_PANEL_3STATE	0x00000001
#define 	SM502_SYSCTL_MEM_3STATE		0x00000002
#define 	SM502_SYSCTL_CRT_3STATE		0x00000004
#define 	SM502_SYSCTL_BURST_32		0x00000000
#define 	SM502_SYSCTL_BURST_64		0x00000010
#define 	SM502_SYSCTL_BURST_128		0x00000020
#define 	SM502_SYSCTL_BURST_256		0x00000030
#define 	SM502_SYSCTL_PCI_CLOCK_RUN_E	0x00000040
#define 	SM502_SYSCTL_PCI_RETRY_E	0x00000080
#define 	SM502_SYSCTL_PCI_LOCK		0x00000800
/* stop drawing engine */
#define 	SM502_SYSCTL_ENGINE_ABORT	0x00003000
#define 	SM502_SYSCTL_BURST_READ_E	0x00008000
#define 	SM502_SYSCTL_ZV_VSYNC_DET	0x00010000
#define 	SM502_SYSCTL_CRT_FLIP_PENDING	0x00020000
#define 	SM502_SYSCTL_ENGINE_BUSY	0x00080000
#define 	SM502_SYSCTL_FIFO_EMPTY		0x00100000
#define 	SM502_SYSCTL_VIDEO_FLIP_PENDING	0x00400000
#define 	SM502_SYSCTL_PANEL_FLIP_PENDING	0x00800000
#define 	SM502_SYSCTL_PCI_LT_E		0x01000000
#define 	SM502_SYSCTL_PCI_BM_E		0x02000000
#define 	SM502_SYSCTL_CSC_BUSY		0x10000000
#define 	SM502_SYSCTL_PCI_BURST_E	0x20000000
#define 	SM502_SYSCTL_DISABLE_HSYNC	0x40000000
#define 	SM502_SYSCTL_DISABLE_VSYNC	0x80000000


/* Video Controller Registers */
#define SM502_PANEL_DISP_CRTL	0x080000
#define		SM502_PDC_8BIT			0x00000000
#define		SM502_PDC_16BIT			0x00000001
#define		SM502_PDC_32BIT			0x00000002
#define		SM502_PDC_DEPTH_MASK		0x00000003
#define		SM502_PDC_PANEL_ENABLE		0x00000004
#define		SM502_PDC_GAMMA_ENABLE		0x00000008
#define		SM502_PDC_HPAN_AUTO		0x00000010
#define		SM502_PDC_HPAN_DIR_LEFT		0x00000000
#define		SM502_PDC_HPAN_DIR_RIGHT	0x00000020
#define		SM502_PDC_VPAN_AUTO		0x00000040
#define		SM502_PDC_VPAN_DIR_UP		0x00000080
#define		SM502_PDC_VPAN_DIR_DOWN		0x00000000
#define		SM502_PDC_TIMING_ENABLE		0x00000100
#define		SM502_PDC_COLORKEY_ENABLE	0x00000200
#define		SM502_PDC_CAPTURE_ZV_0		0x00000400
#define		SM502_PDC_HSYNC_PHASE_LOW	0x00001000
#define		SM502_PDC_HSYNC_PHASE_HIGH	0x00000000
#define		SM502_PDC_VSYNC_PHASE_LOW	0x00002000
#define		SM502_PDC_VSYNC_PHASE_HIGH	0x00000000
#define		SM502_PDC_CLOCK_ACT_LOW		0x00004000
#define		SM502_PDC_CLOCK_ACTIVE_HIGH	0x00000000
#define		SM502_PDC_8BIT_TV_ENABLE	0x00008000
#define		SM502_PDC_FIFO_HWATER_1		0x00000000
#define		SM502_PDC_FIFO_HWATER_3		0x00010000
#define		SM502_PDC_FIFO_HWATER_7		0x00020000
#define		SM502_PDC_FIFO_HWATER_11	0x00030000
#define		SM502_PDC_FIFO_HWATE_MASK	0x00030000
#define		SM502_PDC_TYPE_TFT		0x00000000
#define		SM502_PDC_TYPE_8BIT_STN		0x00040000
#define		SM502_PDC_TYPE_12BIT_STN	0x00080000
#define		SM502_PDC_TYPE_MASK		0x000c0000
#define		SM502_PDC_DITHERING_ENABLE	0x00100000
#define		SM502_PDC_TFT_RGB888		0x00000000
#define		SM502_PDC_TFT_RGB333		0x00200000
#define		SM502_PDC_TFT_RGB444		0x00400000
#define		SM502_PDC_TFT_RGB_MASK		0x00600000
#define		SM502_PDC_DITHER_8_GREY		0x00800000
#define		SM502_PDC_FPVDDEN_HIGH		0x01000000
#define		SM502_PDC_FPVDDEN_LOW		0x00000000
#define		SM502_PDC_PANEL_SIGNALS_ENABLE	0x02000000
#define		SM502_PDC_VBIASEN_HIGH		0x04000000
#define		SM502_PDC_VBIASEN_LOW		0x00000000
#define		SM502_PDC_GPEN_ENABLE		0x08000000

#define SM502_PANEL_PAN_CTRL	0x080004
#define SM502_PANEL_COLOR_KEY	0x080008
#define SM502_PANEL_FB_ADDRESS	0x08000C
#define		SM502_FBA_MASK			0x03fffff0 /* 128bit align */
#define		SM502_FBA_CS1			0x04000000
#define		SM502_FBA_CS0			0x00000000
#define		SM502_FBA_SYSTEM_MEM		0x08000000
#define		SM502_FBA_LOCAL_MEM		0x00000000
#define		SM502_FBA_FLIP_PENDING		0x80000000

#define SM502_PANEL_FB_OFFSET	0x080010
#define		SM502_FBO_FB_STRIDE_MASK	0x00003ff0 /* 128bit align */
#define		SM502_FBA_WIN_STRIDE_MASK	0x3ff00000 /* 128bit align */

#define SM502_PANEL_FB_WIDTH	0x080014
#define		SM502_FBW_WIN_X_MASK		0x00003fff
#define		SM502_FBW_WIN_WIDTH_MASK	0x3fff0000

#define SM502_PANEL_FB_HEIGHT	0x080018
#define		SM502_FBH_WIN_Y_MASK		0x00003fff
#define		SM502_FBH_WIN_HEIGHT_MASK	0x3fff0000
#define SM502_PANEL_TL		0x08001C
#define		SM502_TL_LEFT_MASK		0x000007ff
#define		SM502_TL_TOP_MASK		0x07ff0000

#define SM502_PANEL_BR		0x080020
#define		SM502_BR_RIGHT_MASK		0x000007ff
#define		SM502_BR_BOTTOM_MASK		0x07ff0000

#define SM502_PANEL_HTOTAL	0x080024
#define 	SM502_HT_HDISPE_MASK		0x00000fff
#define 	SM502_HT_HTOTAL_MASK		0x0fff0000
#define SM502_PANEL_HSYNC	0x080028
#define SM502_PANEL_VTOTAL	0x08002C
#define 	SM502_VT_VDISPE_MASK		0x00000fff
#define 	SM502_VT_VTOTAL_MASK		0x0fff0000
#define SM502_PANEL_VSYNC	0x080030

#define SM502_PALETTE_PANEL	0x080400
#define SM502_PALETTE_VIDEO	0x080800
#define SM502_PALETTE_CRT	0x080c00

/* drawing engine */
#define SM502_SRC		0x100000
#define		SM502_SRC_WRAP_ENABLE	0x80000000
#define		SM502_SRC_X_MASK	0x3fff0000
#define		SM502_SRC_Y_MASK	0x0000ffff

#define SM502_DST		0x100004
#define		SM502_DST_WRAP_ENABLE	0x80000000
#define		SM502_DST_X_MASK	0x3fff0000
#define		SM502_DST_Y_MASK	0x0000ffff

#define SM502_DIMENSION		0x100008
#define		SM502_DIM_X_MASK	0x3fff0000
#define		SM502_DIM_Y_MASK	0x0000ffff

#define SM502_CONTROL		0x10000c
#define ROP_COPY 	0x0c
#define ROP_INVERT	0x03
#define		SM502_CTRL_ROP_MASK	0x000000ff
#define		SM502_CTRL_TRANSP_EN	0x00000100
#define		SM502_CTRL_TRANSP_DST	0x00000200
#define		SM502_CTRL_TRANSP_SRC	0x00000000
#define		SM502_CTRL_TRANSP_MATCH	0x00000400
#define		SM502_CTRL_OPAQUE_MATCH	0x00000000
#define		SM502_CTRL_REPEAT_ROT	0x00000800
#define		SM502_CTRL_MONO_PACK_MASK	0x00003000
#define		SM502_CTRL_MONO_PACK_8BIT	0x00001000
#define		SM502_CTRL_MONO_PACK_16BIT	0x00002000
#define		SM502_CTRL_MONO_PACK_32BIT	0x00003000
#define		SM502_CTRL_ROP2_SRC_PAT	0x00004000 /* otherwise src is bmp */
#define		SM502_CTRL_USE_ROP2	0x00008000 /* X-style ROPs vs. Win */
#define		SM502_CTRL_COMMAND_MASK	0x001f0000
#define		SM502_CTRL_CMD_BITBLT	0x00000000
#define		SM502_CTRL_CMD_RECTFILL	0x00010000
#define		SM502_CTRL_CMD_DETILE	0x00020000
#define		SM502_CTRL_CMD_TRAPFILL	0x00030000
#define		SM502_CTRL_CMD_ALPHA	0x00040000
#define		SM502_CTRL_CMD_RLESTRIP	0x00050000
#define		SM502_CTRL_CMD_SHRTSTRK	0x00060000
#define		SM502_CTRL_CMD_LINE	0x00070000
#define		SM502_CTRL_CMD_HOSTWRT	0x00080000
#define		SM502_CTRL_CMD_HOSTREAD	0x00090000
#define		SM502_CTRL_CMD_WRT_BT	0x000a0000
#define		SM502_CTRL_CMD_ROTATE	0x000b0000
#define		SM502_CTRL_CMD_FONT	0x000c0000
#define		SM502_CTRL_CMD_TEXLOAD	0x000f0000
#define		SM502_CTRL_DRAWLAST	0x00200000 /* last pixel in line */
#define		SM502_CTRL_HOSTBLT_MONO	0x00400000 /* colour otherwise */
#define		SM502_CTRL_YSTRETCH_E	0x00800000
#define		SM502_CTRL_Y_STEP_NEG	0x01000000 /* line, otherwise pos */
#define		SM502_CTRL_X_STEP_NEG	0x02000000 /* line, otherwise pos */
#define		SM502_CTRL_LINE_AX_Y	0x04000000 /* otherwise X */
#define		SM502_CTRL_R_TO_L	0x08000000 /* otherwise L to R */
/* run command when writing SM502_DIMENSION */
#define		SM502_CTRL_QUICKSTART_E	0x10000000
#define		SM502_CTRL_UPD_DESTX	0x20000000
#define		SM502_CTRL_PAT_COLOR	0x40000000 /* otherwise mono */
#define		SM502_CTRL_ENGINE_START	0x80000000

#define SM502_PITCH		0x100010
#define		SM502_PITCH_SRC_MASK	0x00003fff
#define		SM502_PITCH_DST_MASK	0x3fff0000

#define SM502_FOREGROUND	0x100014
#define SM502_BACKGROUND	0x100018
#define SM502_STRETCH		0x10001c
#define		SM502_STRETCH_HEIGHT_MASK	0x00000fff /* source */
#define		SM502_STRETCH_ADDR_LINEAR	0x000f0000 /* XY otherwise */
#define		SM502_STRETCH_PIXEL_FORMAT_MASK	0x00300000
#define		SM502_STRETCH_8BIT		0x00000000
#define		SM502_STRETCH_16BIT		0x00100000
#define		SM502_STRETCH_32BIT		0x00200000
#define		SM502_STRETCH_PAT_X_ORIGIN_MASK	0x03800000
#define		SM502_STRETCH_PAT_Y_ORIGIN_MASK 0x38000000
#define		SM502_STRETCH_PAT_XY_ENABLE	0x40000000

#define SM502_COLOR_COMPARE	0x100020
#define SM502_COLOR_COMP_MASK	0x100024
#define SM502_PLANEMASK		0x100028
#define SM502_CLIP_TOP_LEFT	0x10002c
#define		SM501_CLIP_TOP_MASK	0xffff0000
#define		SM501_CLIP_LEFT_MASK	0x00000fff
#define		SM501_CLIP_BLOCK_INSIDE	0x00001000 /* otherwise block outside */
#define		SM501_CLIP_ENABLE	0x00002000

#define SM502_CLIP_BOTTOM_RIGHT	0x100030
#define		SM501_CLIP_BOTTOM_MASK	0xffff0000
#define		SM501_CLIP_RIGHT_MASK	0x00001fff

#define SM502_MONO_PATTERN_0	0x100034
#define SM502_MONO_PATTERN_1	0x100038
#define SM502_WINDOW_WIDTH	0x10003c
#define		SM502_WIN_SRC_MASK	0x00001fff
#define		SM502_WIN_DST_MASK	0x1fff0000

#define SM502_SRC_BASE		0x100040
#define		SM502_SRC_BASE_ADDR_MASK	0x03fffff0 /* 128bit align */
#define		SM502_SRC_BASE_SYSMEM_CS1	0x04000000 /* SC0 otherw. */
#define		SM502_SRC_BASE_SYSMEM		0x08000000 /* local otherw. */

#define SM502_DST_BASE		0x100044
#define		SM502_DST_BASE_ADDR_MASK	0x03fffff0 /* 128bit align */
#define		SM502_DST_BASE_SYSMEM_CS1	0x04000000 /* SC0 otherw. */
#define		SM502_DST_BASE_SYSMEM		0x08000000 /* local otherw. */

#define SM502_ALPHA		0x100048
#define SM502_WRAP		0x10004c
#define		SM502_WRAP_HEIGHT_MASK	0x0000ffff
#define		SM502_WRAP_WIDTH_MASK	0xffff0000

#define SM502_STATUS		0x100050
#define		SM502_CMD_DONE		0x00000001
#define		SM502_CSC_DONE		0x00000002

#define	SM502_DATAPORT		0x110000

#endif /* SM502REG_H */
