#	$NetBSD: Makefile.inc,v 1.10 2009/08/14 19:23:53 dsl Exp $

SRCS+=	milli.S
SRCS+=	bcopy.S memcpy.S memmove.S

# XXX: spcopy does not really belong in libkern in the first place
.ifndef RUMPKERNEL
SRCS+=	spcopy.S
.endif

SRCS+= ashrdi3.c divdi3.c    
SRCS+= ffs.c bswap16.c bswap32.c
