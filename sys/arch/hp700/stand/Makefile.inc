#	$NetBSD: Makefile.inc,v 1.6 2006/06/25 05:11:42 mrg Exp $

BINDIR=		/usr/mdec

.include <bsd.own.mk>

.if ${HAVE_GCC} > 3
COPTS+=		-Wno-pointer-sign
.endif
COPTS+=		-fno-strict-aliasing
