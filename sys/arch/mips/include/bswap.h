/*      $NetBSD: bswap.h,v 1.2 1999/08/21 05:39:53 simonb Exp $      */

#ifndef _MIPS_BSWAP_H_
#define	_MIPS_BSWAP_H_

#define __BSWAP_RENAME
#include <sys/bswap.h>

#endif /* !_MIPS_BSWAP_H_ */
