/*	$NetBSD$	*/

#ifndef _ALGOR_DB_MACHDEP_H_
#define _ALGOR_DB_MACHDEP_H_

#define DB_ELF_SYMBOLS
#define DB_ELFSIZE	32

#include <mips/db_machdep.h>

#endif	/* !_ALGOR_DB_MACHDEP_H_ */
