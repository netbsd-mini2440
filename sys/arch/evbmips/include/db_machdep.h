/*	$NetBSD: db_machdep.h,v 1.3 2000/01/09 15:34:42 ad Exp $	*/

#ifndef _EBVMIPS_DB_MACHDEP_H_
#define _EBVMIPS_DB_MACHDEP_H_

/* XXX: Common for all evbmips or not? */
#define DB_ELF_SYMBOLS
#define DB_ELFSIZE	32

#include <mips/db_machdep.h>

#endif	/* !_EBVMIPS_DB_MACHDEP_H_ */
