/*	$NetBSD: vmparam.h,v 1.1 2002/03/07 14:44:02 simonb Exp $	*/

#ifndef _EVBMIPS_VMPARAM_H_
#define _EVBMIPS_VMPARAM_H_

#include <mips/vmparam.h>

#define	VM_PHYSSEG_MAX		32
 
#endif	/* !_EVBMIPS_VMPARAM_H_ */
