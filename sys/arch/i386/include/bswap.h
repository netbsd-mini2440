/*      $NetBSD: bswap.h,v 1.3 2006/01/31 07:49:18 dsl Exp $      */

/* Written by Manuel Bouyer. Public domain */

#ifndef _I386_BSWAP_H_
#define	_I386_BSWAP_H_

#include <machine/byte_swap.h>

#define __BSWAP_RENAME
#include <sys/bswap.h>

#endif /* !_I386_BSWAP_H_ */
