/*	$NetBSD: comio_direct.h,v 1.3 2003/04/16 14:58:32 dsl Exp $	*/

int cominit_d(int, int);
int computc_d(int, int);
int comgetc_d(int);
int comstatus_d(int);
