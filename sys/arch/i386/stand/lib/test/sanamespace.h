/*	$NetBSD: biosdisk.c,v 1.8 1998/02/19 14:12:48 drochner Exp $	*/

/* take back the namespace mangling done by "Makefile.satest" */

#undef main
#undef exit
#undef free
#undef open
#undef close
#undef read
#undef write
#undef ioctl
#undef lseek
#undef printf
#undef sprintf
#undef vprintf
#undef putchar
#undef gets
#undef strerror
#undef errno
