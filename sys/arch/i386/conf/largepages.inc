#	$NetBSD: largepages.inc,v 1.2 2007/10/18 15:28:34 yamt Exp $
#
# Options to create a kernel suitable for mapping with large
# pages.
#

makeoptions 	KERN_LDSCRIPT="kern.ldscript.4MB"
