/*	$NetBSD: hd64461.h,v 1.1.18.3 2004/09/21 13:15:54 skrll Exp $	*/

#ifndef _HPCBOOT_SH_DEV_HD64461_H_
#define	_HPCBOOT_SH_DEV_HD64461_H_

#include "../../../../../hpcsh/dev/hd64461/hd64461reg.h"
#include "../../../../../hpcsh/dev/hd64461/hd64461intcreg.h"
#include "../../../../../hpcsh/dev/hd64461/hd64461pcmciareg.h"
#include "../../../../../hpcsh/dev/hd64461/hd64461gpioreg.h"
#include "../../../../../hpcsh/dev/hd64461/hd64461uartreg.h"
#include "../../../../../hpcsh/dev/hd64461/hd64461uartvar.h"

#endif /* _HPCBOOT_SH_DEV_HD64461_H_ */
