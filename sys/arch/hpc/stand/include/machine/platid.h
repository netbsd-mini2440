/*	$NetBSD: platid.h,v 1.2.16.3 2004/09/21 13:15:59 skrll Exp $	*/

#ifdef MIPS
#define	hpcmips
#endif
#ifdef SHx
#define	hpcsh
#endif
#ifdef ARM
#define	hpcarm
#endif
#include "../../../include/platid.h"
