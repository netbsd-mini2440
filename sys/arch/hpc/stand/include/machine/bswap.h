/*	$NetBSD: bswap.h,v 1.1.26.3 2004/09/21 13:15:59 skrll Exp $	*/

#ifndef _MACHINE_BSWAP_H_
#define	_MACHINE_BSWAP_H_

#define	__BSWAP_RENAME
#include <sys/bswap.h>

#endif // _MACHINE_BSWAP_H_
