/*	$NetBSD: bus.h,v 1.25 2007/10/17 19:55:31 garbled Exp $	*/

#define PHYS_TO_BUS_MEM(t,x)	(x)
#define BUS_MEM_TO_PHYS(t,x)	(x)

#include <powerpc/bus.h>
#include <powerpc/ofw_bus.h>
