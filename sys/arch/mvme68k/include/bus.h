/*	$NetBSD: bus.h,v 1.2.16.1 2000/03/11 20:51:51 scw Exp $ */

#include <machine/bus_space.h>
#include <machine/bus_dma.h>

#define generic_btop(x) m68k_btop(x)
