/*	$NetBSD: db_machdep.h,v 1.1.1.1 1995/07/25 23:12:14 chuck Exp $	*/

/* Just use the common m68k definition */
#include <m68k/db_machdep.h>

#undef DB_AOUT_SYMBOLS
#define DB_ELF_SYMBOLS
#define DB_ELFSIZE 32
