/*	$NetBSD: disklabel.h,v 1.1.36.1 2005/11/10 13:57:47 skrll Exp $	*/

#if HAVE_NBTOOL_CONFIG_H
#include <nbinclude/sh3/disklabel.h>
#else
#include <sh3/disklabel.h>
#endif /* HAVE_NBTOOL_CONFIG_H */
