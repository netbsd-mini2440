/*	$NetBSD: vmparam.h,v 1.1 1999/09/13 10:31:00 itojun Exp $	*/

#include <sh3/vmparam.h>

#define VM_PHYSSEG_MAX		1

#define VM_NFREELIST		1
#define VM_FREELIST_DEFAULT	0
