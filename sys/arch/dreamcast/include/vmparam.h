/*	$NetBSD: vmparam.h,v 1.1 2000/12/11 18:19:16 marcus Exp $	*/
#ifndef _DREAMCAST_VMPARAM_H_
#define _DREAMCAST_VMPARAM_H_

#include <sh3/vmparam.h>

#define VM_PHYSSEG_MAX		1

#define VM_NFREELIST		1
#define VM_FREELIST_DEFAULT	0

#endif /* _DREAMCAST_VMPARAM_H_ */
