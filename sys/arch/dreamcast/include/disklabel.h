/*	$NetBSD: disklabel.h,v 1.1.26.1 2005/11/10 13:55:51 skrll Exp $	*/
#ifndef _DREAMCAST_DISKLABEL_H_
#define _DREAMCAST_DISKLABEL_H_

#if HAVE_NBTOOL_CONFIG_H
#include <nbinclude/sh3/disklabel.h>
#else
#include <sh3/disklabel.h>
#endif /* HAVE_NBTOOL_CONFIG_H */

#endif /* _DREAMCAST_DISKLABEL_H_ */
