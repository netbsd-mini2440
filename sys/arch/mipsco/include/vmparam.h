/*	$NetBSD: vmparam.h,v 1.1 2000/08/12 22:58:48 wdk Exp $	*/

#include <mips/vmparam.h>

/*
 * One physical memory segment
 */
#define	VM_PHYSSEG_MAX		1
