/*	$NetBSD: db_machdep.h,v 1.1 2000/08/12 22:58:12 wdk Exp $	*/

#ifndef _MIPSCO_DB_MACHDEP_H_
#define _MIPSCO_DB_MACHDEP_H_

#define DB_ELF_SYMBOLS
#define DB_ELFSIZE	32

#include <mips/db_machdep.h>

#endif	/* !_MIPSCO_DB_MACHDEP_H_ */
