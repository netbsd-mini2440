/*	$NetBSD: db_machdep.h,v 1.1 2000/06/14 15:39:57 soren Exp $	*/

#ifndef _SGIMIPS_DB_MACHDEP_H_
#define _SGIMIPS_DB_MACHDEP_H_

#define DB_ELF_SYMBOLS
#define DB_ELFSIZE	32

#include <mips/db_machdep.h>

#endif	/* !_SGIMIPS_DB_MACHDEP_H_ */
