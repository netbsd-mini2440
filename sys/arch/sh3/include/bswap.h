/*      $NetBSD: bswap.h,v 1.6 2006/01/31 07:58:56 dsl Exp $      */

#ifndef _SH3_BSWAP_H_
#define	_SH3_BSWAP_H_

#include <sh3/byte_swap.h>

#include <sys/bswap.h>

#endif /* !_SH3_BSWAP_H_ */
