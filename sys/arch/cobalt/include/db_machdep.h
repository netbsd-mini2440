/*	$NetBSD: db_machdep.h,v 1.1 2000/03/19 23:07:46 soren Exp $	*/

#ifndef _COBALT_DB_MACHDEP_H_
#define _COBALT_DB_MACHDEP_H_

#define DB_ELF_SYMBOLS
#define DB_ELFSIZE	32

#include <mips/db_machdep.h>

#endif	/* !_COBALT_DB_MACHDEP_H_ */
