/*      $NetBSD: bswap.h,v 1.3 2002/08/14 15:08:57 thorpej Exp $      */

#ifndef _MACHINE_BSWAP_H_
#define	_MACHINE_BSWAP_H_

#include <arm/byte_swap.h>

#define __BSWAP_RENAME
#include <sys/bswap.h>

#endif /* !_MACHINE_BSWAP_H_ */
