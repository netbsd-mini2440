/*	$NetBSD: bootinfo.h,v 1.2 2003/02/26 21:29:34 fvdl Exp $	*/

#ifndef _AMD64_BOOTINFO_H
#define _AMD64_BOOTINFO_H

#include <x86/bootinfo.h>

#define VAR32_SIZE	4096

#endif	/* _AMD64_BOOTINFO_H_ */
