# $NetBSD: Makefile.inc,v 1.2 2009/11/25 17:08:08 tron Exp $

CFLAGS+=	-mno-red-zone

# this should really be !(RUMPKERNEL && PIC)
.if !defined(RUMPKERNEL)
CFLAGS+=	-mcmodel=kernel
.endif

USE_SSP?=	yes
