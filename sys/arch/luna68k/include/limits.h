/*	$NetBSD: limits.h,v 1.1.1.1 1995/07/25 23:12:16 chuck Exp $	*/

#ifndef _MACHINE_LIMITS_H_
#define _MACHINE_LIMITS_H_

/* Just use the common m68k definition */
#include <m68k/limits.h>

#endif /* _MACHINE_LIMITS_H_ */
