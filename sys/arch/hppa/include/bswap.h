/*      $NetBSD: bswap.h,v 1.1 2002/06/05 01:04:21 fredette Exp $	*/

/* Written by Manuel Bouyer. Public domain */

#ifndef _HPPA_BSWAP_H_
#define	_HPPA_BSWAP_H_

#include <machine/byte_swap.h>

#define __BSWAP_RENAME
#include <sys/bswap.h>

#endif /* !_HPPA_BSWAP_H_ */
