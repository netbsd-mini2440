/* $NetBSD: types.h,v 1.12 2007/10/17 19:54:28 garbled Exp $ */

#ifndef _HPCARM_TYPES_H_
#define	_HPCARM_TYPES_H_

#include <arm/arm32/types.h>

#define	__HAVE_DEVICE_REGISTER
#define	__GENERIC_SOFT_INTERRUPTS_ALL_LEVELS

#endif
