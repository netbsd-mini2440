/* $NetBSD: db_machdep.h,v 1.2 2001/02/23 21:23:52 reinoud Exp $ */

#include <arm/db_machdep.h>

/* hpcarm uses ELF for kernel */
#define DB_ELF_SYMBOLS
#define DB_ELFSIZE      32
