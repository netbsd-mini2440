/*	$NetBSD: vmparam.h,v 1.2 2001/01/28 03:31:32 uch Exp $	*/
#ifndef _HPCSH_VMPARAM_H_
#define _HPCSH_VMPARAM_H_

#include <sh3/vmparam.h>

#define VM_PHYSSEG_MAX		3

#define VM_NFREELIST		1
#define VM_FREELIST_DEFAULT	0

#endif /* _HPCSH_VMPARAM_H_ */
