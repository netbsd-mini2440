/*	$NetBSD: disklabel.h,v 1.2.26.1 2005/11/10 13:56:31 skrll Exp $	*/
#ifndef _HPCSH_DISKLABEL_H_
#define _HPCSH_DISKLABEL_H_

#if HAVE_NBTOOL_CONFIG_H
#include <nbinclude/sh3/disklabel.h>
#else
#include <sh3/disklabel.h>
#endif /* HAVE_NBTOOL_CONFIG_H */

#endif /* _HPCSH_DISKLABEL_H_ */
