/*	$NetBSD: db_machdep.h,v 1.3 2001/09/04 07:12:28 simonb Exp $	*/

#ifndef _ARC_DB_MACHDEP_H_
#define _ARC_DB_MACHDEP_H_

#define DB_ELF_SYMBOLS
#define DB_ELFSIZE      32

#include <mips/db_machdep.h>

#endif	/* !_ARC_DB_MACHDEP_H_ */
