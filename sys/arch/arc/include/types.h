/*	$NetBSD: types.h,v 1.21 2007/10/17 19:53:28 garbled Exp $	*/

#define	_MIPS_PADDR_T_64BIT

#include <mips/types.h>

#define	__HAVE_DEVICE_REGISTER

#define	__HAVE_MIPS_MACHDEP_CACHE_CONFIG
