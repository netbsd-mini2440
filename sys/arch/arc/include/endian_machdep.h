/*	$NetBSD: endian.h,v 1.8 2000/03/16 15:09:35 mycroft Exp $	*/

#define _BYTE_ORDER _LITTLE_ENDIAN
#include <mips/endian_machdep.h>
