/*      $NetBSD: bswap.h,v 1.3 2001/03/30 20:00:05 leo Exp $      */

#ifndef _MACHINE_BSWAP_H_
#define	_MACHINE_BSWAP_H_

#include <m68k/byte_swap.h>

#define	__BSWAP_RENAME
#include <sys/bswap.h>

#endif /* !_MACHINE_BSWAP_H_ */
