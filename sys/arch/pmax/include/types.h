/*	$NetBSD: types.h,v 1.24 2008/01/03 23:02:24 joerg Exp $	*/

#include <mips/types.h>

#define	__HAVE_DEVICE_REGISTER

/* MIPS specific options */
#define	__HAVE_BOOTINFO_H
#define	__HAVE_MIPS_MACHDEP_CACHE_CONFIG
