/*	$NetBSD: db_machdep.h,v 1.2 1999/03/23 22:07:07 simonb Exp $	*/

#ifndef _PMAX_DB_MACHDEP_H_
#define _PMAX_DB_MACHDEP_H_

#define DB_ELF_SYMBOLS
#define DB_ELFSIZE	32
#define DB_AOUT_SYMBOLS

#include <mips/db_machdep.h>

#endif	/* !_PMAX_DB_MACHDEP_H_ */
