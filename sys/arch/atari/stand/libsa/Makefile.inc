# $NetBSD$

.PATH.c: ${SA_EXTRADIR}
.PATH.S: ${SA_EXTRADIR}

# NetBSD/atari specific replacements: dev.c

# machine dependant routines
SRCS+=   consio.S dev.c diskio.c
