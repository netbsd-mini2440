/*	$NetBSD: types.h,v 1.11 2007/10/17 19:55:55 garbled Exp $	*/

#include <mips/types.h>

/* MIPS specific options */
#define	__HAVE_BOOTINFO_H
#define	__HAVE_MIPS_MACHDEP_CACHE_CONFIG
