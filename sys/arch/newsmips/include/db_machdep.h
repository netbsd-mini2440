/*	$NetBSD: db_machdep.h,v 1.2 1999/03/26 07:03:01 tsubai Exp $	*/

#ifndef _NEWSMIPS_DB_MACHDEP_H_
#define _NEWSMIPS_DB_MACHDEP_H_

#define DB_ELF_SYMBOLS
#define DB_ELFSIZE	32

#include <mips/db_machdep.h>

#endif	/* !_NEWSMIPS_DB_MACHDEP_H_ */
