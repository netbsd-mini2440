/*      $NetBSD: bswap.h,v 1.1 1999/01/15 13:31:22 bouyer Exp $      */

#ifndef _MACHINE_BSWAP_H_
#define	_MACHINE_BSWAP_H_

#define __BSWAP_RENAME
#include <sys/bswap.h>

#endif /* !_MACHINE_BSWAP_H_ */
