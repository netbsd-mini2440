/*	$NetBSD: cdefs.h,v 1.6 1999/03/20 01:40:26 thorpej Exp $	*/

#ifndef	_MACHINE_CDEFS_H_
#define	_MACHINE_CDEFS_H_

/* We're elf only: inspected by sys/cdefs.h  */
#ifndef __ELF__
#define __ELF__
#endif

#endif /* !_MACHINE_CDEFS_H_ */
