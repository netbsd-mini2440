/*	$NetBSD: spkr.h,v 1.1 1997/10/14 06:48:44 sakamoto Exp $	*/

/*
 * spkr.h -- interface definitions for speaker ioctl()
 */

#ifndef _BEBOX_SPKR_H_
#define _BEBOX_SPKR_H_

#include <sys/ioctl.h>

#include <dev/isa/spkrio.h>

#endif /* _BEBOX_SPKR_H_ */
