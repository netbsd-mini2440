/*      $NetBSD: bswap.h,v 1.3 2000/05/27 16:44:14 ragge Exp $      */

/* Written by Manuel Bouyer. Public domain */

#ifndef _MACHINE_BSWAP_H_
#define	_MACHINE_BSWAP_H_

#include <machine/byte_swap.h>

#define __BSWAP_RENAME
#include <sys/bswap.h>

#endif /* !_MACHINE_BSWAP_H_ */
