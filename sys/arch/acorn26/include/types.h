/* $NetBSD: types.h,v 1.10 2007/10/17 19:52:52 garbled Exp $ */

#ifndef	_ACORN26_TYPES_H_
#define	_ACORN26_TYPES_H_

#include <arm/arm26/types.h>

#define	__GENERIC_SOFT_INTERRUPTS_ALL_LEVELS

#endif	/* _ACORN26_TYPES_H_ */
