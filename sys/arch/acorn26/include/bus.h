/* $NetBSD: bus.h,v 1.2 2002/03/24 23:37:43 bjh21 Exp $ */

#ifndef _ACORN26_BUS_H
#define _ACORN26_BUS_H

#include <arm/bus.h>

extern struct bus_space iobus_bs_tag;

extern int bus_space_shift(bus_space_tag_t, int, bus_space_tag_t *);

#endif
