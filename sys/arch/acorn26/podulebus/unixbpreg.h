/* $NetBSD: unixbpreg.h,v 1.1 2000/05/09 21:56:04 bjh21 Exp $ */

/*
 * This file is in the public domain.
 */

/* Acorn "Unix Backplane" registers */

#ifndef _UNIXBPREG_H_
#define _UNIXBPREG_H_

#define UNIXBP_REG_REQUEST	0 /* Enabled _and_ active */
#define UNIXBP_REG_MASK		1

#endif
