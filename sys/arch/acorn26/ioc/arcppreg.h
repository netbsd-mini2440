/* $NetBSD: arcppreg.h,v 1.1 2001/04/22 15:01:25 bjh21 Exp $ */

/*
 * This file is in the public domain.
 */

/* Archimedes parallel port registers */

/* Offset of data latch from IOC bank base (bus_size_t units) */
#define ARCPP_DATA	0x04
