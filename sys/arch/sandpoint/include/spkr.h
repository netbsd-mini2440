/*	$NetBSD: spkr.h,v 1.2 1998/04/15 21:53:47 drochner Exp $	*/

/*
 * spkr.h -- interface definitions for speaker ioctl()
 */

#ifndef _SANDPOINT_SPKR_H_
#define _SANDPOINT_SPKR_H_

#include <sys/ioctl.h>

#include <dev/isa/spkrio.h>

#endif /* _SANDPOINT_SPKR_H_ */
