/*	$NetBSD: types.h,v 1.21 2008/01/06 18:50:32 mhitch Exp $	*/

#ifndef _MACHINE_TYPES_H_
#define	_MACHINE_TYPES_H_

#include <m68k/types.h>

#define	__GENERIC_SOFT_INTERRUPTS_ALL_LEVELS

#endif
