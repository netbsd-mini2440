/*	$NetBSD: db_machdep.h,v 1.4 1994/10/26 02:06:03 cgd Exp $	*/

#ifndef _MACHINE_DB_MACHDEP_H_
#define _MACHINE_DB_MACHDEP_H_

#include <m68k/db_machdep.h>

#define DB_ELF_SYMBOLS
#define DB_ELFSIZE	32

#endif
