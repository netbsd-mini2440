/*	$NetBSD: db_machdep.h,v 1.1.1.1 1996/05/05 12:17:11 oki Exp $	*/

/* Just use the common m68k definition */
#include <m68k/db_machdep.h>

#define	DB_ELF_SYMBOLS
#define	DB_ELFSIZE	32
