/*	$NetBSD: disklabel.h,v 1.1.36.1 2005/11/10 13:56:05 skrll Exp $	*/
#ifndef _EVBSH3_DISKLABEL_H_
#define _EVBSH3_DISKLABEL_H_

#if HAVE_NBTOOL_CONFIG_H
#include <nbinclude/sh3/disklabel.h>
#else
#include <sh3/disklabel.h>
#endif /* HAVE_NBTOOL_CONFIG_H */

#endif /* _EVBSH3_DISKLABEL_H_ */
