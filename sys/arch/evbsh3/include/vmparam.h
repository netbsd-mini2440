/*	$NetBSD: vmparam.h,v 1.1 1999/09/13 10:30:45 itojun Exp $	*/
#ifndef _EVBSH3_VMPARAM_H_
#define _EVBSH3_VMPARAM_H_

#include <sh3/vmparam.h>

#define VM_PHYSSEG_MAX		1

#define VM_NFREELIST		1
#define VM_FREELIST_DEFAULT	0

#endif /* _EVBSH3_VMPARAM_H_ */
