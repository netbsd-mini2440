/*	$NetBSD: tspldvar.h,v 1.1.4.1 2005/01/17 19:29:23 skrll Exp $	*/

#ifndef _TSPLDVAR_H_
#define _TSPLDVAR_H_

struct tspld_attach_args {
	bus_space_tag_t	ta_iot;
};

#endif /* _TS7XXXVAR_H_ */
