/*	$NetBSD: param.h,v 1.2 2005/12/11 12:17:51 christos Exp $	*/

#ifndef _IYONIX_PARAM_H_
#define _IYONIX_PARAM_H_

#define	_MACHINE	iyonix
#define	MACHINE		"iyonix"

#include <arm/arm32/param.h>

#endif /* _IYONIX_PARAM_H_ */
