/*	$NetBSD: disklabel.h,v 1.1.2.2 2005/11/10 13:57:09 skrll Exp $	*/

#if HAVE_NBTOOL_CONFIG_H
#include <nbinclude/arm/disklabel.h>
#else
#include <arm/disklabel.h>
#endif /* HAVE_NBTOOL_CONFIG_H */
