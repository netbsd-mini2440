/*	$NetBSD: db_machdep.h,v 1.1.1.1 1999/09/16 12:23:22 takemura Exp $	*/

#ifndef _HPCMIPS_DB_MACHDEP_H_
#define _HPCMIPS_DB_MACHDEP_H_

#define DB_ELF_SYMBOLS
#define DB_ELFSIZE      32

#include <mips/db_machdep.h>

#endif	/* !_HPCMIPS_DB_MACHDEP_H_ */
