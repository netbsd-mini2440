/*	$NetBSD: disklabel.h,v 1.2.24.1 2005/11/10 13:59:38 skrll Exp $	*/

#if HAVE_NBTOOL_CONFIG_H
#include <nbinclude/sun68k/disklabel.h>
#else
#include <sun68k/disklabel.h>
#endif /* HAVE_NBTOOL_CONFIG_H */
