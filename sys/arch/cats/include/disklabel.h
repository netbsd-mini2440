/*	$NetBSD: disklabel.h,v 1.3.16.1 2005/11/10 13:55:47 skrll Exp $	*/

#if HAVE_NBTOOL_CONFIG_H
#include <nbinclude/arm/disklabel.h>
#else
#include <arm/disklabel.h>
#endif /* HAVE_NBTOOL_CONFIG_H */
