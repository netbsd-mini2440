/*	$NetBSD: bus.h,v 1.11 2007/10/17 19:56:07 garbled Exp $	*/

#define PHYS_TO_BUS_MEM(t,x)	(x)
#define BUS_MEM_TO_PHYS(t,x)	(x)

#include <powerpc/ofw_bus.h>
#include <powerpc/bus.h>
