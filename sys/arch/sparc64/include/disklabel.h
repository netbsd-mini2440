/*       $NetBSD: disklabel.h,v 1.2.6.1 2005/11/10 13:59:18 skrll Exp $        */

#if HAVE_NBTOOL_CONFIG_H
#include <nbinclude/sparc/disklabel.h>
#else
#include <sparc/disklabel.h>
#endif /* HAVE_NBTOOL_CONFIG_H */
