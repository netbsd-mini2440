/*	$NetBSD: md2.h,v 1.1 2002/09/13 19:07:01 thorpej Exp $	*/

/* We unconditionally use the NetBSD MD2 in libnbcompat. */
#include "nbtool_config.h"
#include "../../include/md2.h"
