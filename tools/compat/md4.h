/*	$NetBSD: md4.h,v 1.2 2003/10/01 11:59:06 seb Exp $	*/

/* We unconditionally use the NetBSD MD4 in libnbcompat. */
#include "nbtool_config.h"
#include "../../sys/sys/md4.h"
