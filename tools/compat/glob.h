/*	$NetBSD: glob.h,v 1.1 2002/09/13 19:07:01 thorpej Exp $	*/

/* We unconditionally use the NetBSD glob(3) in libnbcompat. */
#include "nbtool_config.h"
#include "../../include/glob.h"
