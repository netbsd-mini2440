#	$NetBSD: Makefile.inc,v 1.179 2007/11/12 23:11:58 ad Exp $
#	@(#)Makefile.inc	8.3 (Berkeley) 10/24/94

.PATH: ${.CURDIR}/misc

# constructor
SRCS+=	initfini.c

# for -fstack-protector
SRCS+=	stack_protector.c
