/*	$NetBSD: errno.c,v 1.4 2000/02/17 21:34:21 kleink Exp $	*/

#include <sys/cdefs.h>
#if defined(LIBC_SCCS) && !defined(lint)
__RCSID("$NetBSD$");
#endif /* LIBC_SCCS and not lint */

int errno;
