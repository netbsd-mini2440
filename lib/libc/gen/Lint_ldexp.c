/*	$NetBSD: Lint_ldexp.c,v 1.1 1997/12/07 00:24:50 matthias Exp $	*/

/*
 * This file placed in the public domain.
 * Matthias Pfaller, December 6, 1997.
 */

#include <math.h>

/*ARGSUSED*/
double
ldexp(val, exp)
	double val;
	int exp;
{
	return(0);
}
