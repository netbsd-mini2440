/*	$NetBSD: Lint_frexp.c,v 1.1 1997/12/07 00:24:50 matthias Exp $	*/

/*
 * This file placed in the public domain.
 * Matthias Pfaller, December 6, 1997.
 */

#include <math.h>

/*ARGSUSED*/
double
frexp(value, eptr)
	double value;
	int *eptr;
{
	return(0);
}
