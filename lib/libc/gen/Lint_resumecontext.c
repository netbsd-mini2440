/*	$NetBSD: Lint_resumecontext.c,v 1.1.2.1 2001/03/05 23:34:38 nathanw Exp $	*/

/*
 * This file placed in the public domain.
 * Klaus Klein, January 26, 1999.
 */

#include <ucontext.h>

/*ARGSUSED*/
void
_resumecontext()
{
}
