/*	$NetBSD: _daylight.c,v 1.3 2005/07/30 15:21:21 christos Exp $	*/

/*
 * Written by Klaus Klein, December 27, 2000.
 * Public domain.
 */

#include <sys/cdefs.h>
#if defined(LIBC_SCCS) && !defined(lint)
__RCSID("$NetBSD: _daylight.c,v 1.3 2005/07/30 15:21:21 christos Exp $");
#endif /* LIBC_SCCS and not lint */

#if defined(__indr_reference)
__indr_reference(_daylight, daylight)
#endif
/* LINTED empty translation unit */
