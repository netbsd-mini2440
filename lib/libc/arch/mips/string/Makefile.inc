#	$NetBSD: Makefile.inc,v 1.4 2009/07/30 20:57:16 dsl Exp $

SRCS+=	bcmp.S bcopy.S bzero.S \
	ffs.S \
	memcpy.S memmove.S \
	strchr.S strrchr.S \
	strcmp.S strlen.S
