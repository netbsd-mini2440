#	$NetBSD: Makefile.inc,v 1.3 2006/09/22 17:59:47 cherry Exp $

SRCS+=	bswap16.c bswap32.c bswap64.c

SRCS+= flt_rounds.c fpgetround.c fpsetround.c fpgetmask.c fpsetmask.c
SRCS+=	isinff_ieee754.c isinfd_ieee754.c
SRCS+=	isnanf_ieee754.c isnand_ieee754.c
