/*	$NetBSD: dkcksum.h,v 1.4 2009/10/24 18:15:45 tsutsui Exp $	*/

uint16_t	dkcksum(struct disklabel *);
uint16_t	dkcksum_sized(struct disklabel *, size_t);
