/* $NetBSD$ */

/* 
 * This file is placed in the public domain.
 */

#ifndef _SECMODEL_EXAMPLE_EXAMPLE_H_
#define	_SECMODEL_EXAMPLE_EXAMPLE_H_

void secmodel_example_init(void);
void secmodel_example_start(void);

#endif /* !_SECMODEL_EXAMPLE_EXAMPLE_H_ */
