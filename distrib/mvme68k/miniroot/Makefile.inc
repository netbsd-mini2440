#	$NetBSD: Makefile.inc,v 1.9 2009/09/20 22:46:12 abs Exp $

IMAGESIZE=	7m
MAKEFS_FLAGS=	-o density=4096
IMAGEENDIAN=	be
MAKEDEVTARGETS=	all
IMAGEDEPENDS+=	${ARCHDIR}/dot.profile ${ARCHDIR}/install.md \
		${KERNOBJDIR}/GENERIC/netbsd \
		${DISTRIBDIR}/common/termcap
LISTS+=		${DISTRIBDIR}/common/list.sysinst
