#	$NetBSD: Makefile.inc,v 1.1 2009/06/07 22:38:45 christos Exp $

LDADD+=	-lssh -lcrypto -lcrypt -lz
DPADD+=	${LIBSSH} ${LIBCRYPTO} ${LIBCRYPT} ${LIBZ}

.include "${.PARSEDIR}/../Makefile.inc"
