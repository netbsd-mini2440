#	$NetBSD: comp.inc,v 1.6 2007/12/09 22:44:14 adrianp Exp $
#
#	@(#) Copyright (c) 1995 Simon J. Gerraty
#
#	SRCS extracted from src/crypto/dist/openssl/crypto/comp/Makefile
#

.PATH:	${OPENSSLSRC}/crypto/modes


MODES_SRCS = cbc128.c cfb128.c ctr128.c cts128.c ofb128.c
SRCS += ${MODES_SRCS}

.for cryptosrc in ${MODES_SRCS}
CPPFLAGS.${cryptosrc} = -I${OPENSSLSRC}/crypto/modes
.endfor
