#	$NetBSD: Makefile.compat,v 1.3 2008/10/28 22:58:23 mrg Exp $

#
# Makefile fragment to help implement a set of 'cc -m32' libraries.
#

COPTS+=			-m32
CPUFLAGS+=		-m32
LDADD+=			-m32
LDFLAGS+=		-m32
MKDEPFLAGS+=		-m32

.include "Makefile.compat"
