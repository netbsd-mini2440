#	$NetBSD: bsd.i386.mk,v 1.1 2009/12/13 09:07:32 mrg Exp $

LD+=			-m elf_i386
MLIBDIR=		i386
LIBC_MACHINE_ARCH=	${MLIBDIR}
COMMON_MACHINE_ARCH=	${MLIBDIR}
KVM_MACHINE_ARCH=	${MLIBDIR}
PTHREAD_MACHINE_ARCH=	${MLIBDIR}
BFD_MACHINE_ARCH=	${MLIBDIR}
CSU_MACHINE_ARCH=	${MLIBDIR}
CRYPTO_MACHINE_CPU=	${MLIBDIR}
LDELFSO_MACHINE_CPU=	${MLIBDIR}

.include "${.PARSEDIR}/../../m32.mk"
