#	$NetBSD: bsd.sparc.mk,v 1.1 2009/12/13 09:07:32 mrg Exp $

LD+=			-m elf32_sparc
MLIBDIR=		sparc
LIBC_MACHINE_ARCH=	${MLIBDIR}
COMMON_MACHINE_ARCH=	${MLIBDIR}
KVM_MACHINE_ARCH=	${MLIBDIR}
PTHREAD_MACHINE_ARCH=	${MLIBDIR}
BFD_MACHINE_ARCH=	${MLIBDIR}
CSU_MACHINE_ARCH=	${MLIBDIR}
CRYPTO_MACHINE_CPU=	${MLIBDIR}
LDELFSO_MACHINE_CPU=	${MLIBDIR}

.include "${.PARSEDIR}/../../m32.mk"
