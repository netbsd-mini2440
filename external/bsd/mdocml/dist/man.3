.\"	$Vendor-Id: man.3,v 1.10 2009/10/03 16:36:06 kristaps Exp $
.\"
.\" Copyright (c) 2009 Kristaps Dzonsons <kristaps@kth.se>
.\"
.\" Permission to use, copy, modify, and distribute this software for any
.\" purpose with or without fee is hereby granted, provided that the above
.\" copyright notice and this permission notice appear in all copies.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
.\" WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
.\" MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
.\" ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
.\" WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
.\" ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
.\" OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
.\"
.Dd $Mdocdate$
.Dt MAN 3
.Os
.\" SECTION
.Sh NAME
.Nm man_alloc ,
.Nm man_parseln ,
.Nm man_endparse ,
.Nm man_node ,
.Nm man_meta ,
.Nm man_free ,
.Nm man_reset
.Nd man macro compiler library
.\" SECTION
.Sh SYNOPSIS
.In man.h
.Vt extern const char * const * man_macronames;
.Ft "struct man *"
.Fn man_alloc "void *data" "int pflags" "const struct man_cb *cb"
.Ft void
.Fn man_reset "struct man *man"
.Ft void
.Fn man_free "struct man *man"
.Ft int
.Fn man_parseln "struct man *man" "int line" "char *buf"
.Ft "const struct man_node *"
.Fn man_node "const struct man *man"
.Ft "const struct man_meta *"
.Fn man_meta "const struct man *man"
.Ft int
.Fn man_endparse "struct man *man"
.\" SECTION
.Sh DESCRIPTION
The
.Nm man
library parses lines of
.Xr man 7
input (and
.Em only
man) into an abstract syntax tree (AST).
.\" PARAGRAPH
.Pp
In general, applications initiate a parsing sequence with
.Fn man_alloc ,
parse each line in a document with
.Fn man_parseln ,
close the parsing session with
.Fn man_endparse ,
operate over the syntax tree returned by
.Fn man_node
and
.Fn man_meta ,
then free all allocated memory with
.Fn man_free .
The
.Fn man_reset
function may be used in order to reset the parser for another input
sequence.  See the
.Sx EXAMPLES
section for a full example.
.\" PARAGRAPH
.Pp
This section further defines the
.Sx Types ,
.Sx Functions
and
.Sx Variables
available to programmers.  Following that, the
.Sx Abstract Syntax Tree
section documents the output tree.
.\" SUBSECTION
.Ss Types
Both functions (see
.Sx Functions )
and variables (see
.Sx Variables )
may use the following types:
.Bl -ohang -offset "XXXX"
.\" LIST-ITEM
.It Vt struct man
An opaque type defined in
.Pa man.c .
Its values are only used privately within the library.
.\" LIST-ITEM
.It Vt struct man_cb
A set of message callbacks defined in
.Pa man.h .
.\" LIST-ITEM
.It Vt struct man_node
A parsed node.  Defined in
.Pa man.h .
See
.Sx Abstract Syntax Tree
for details.
.El
.\" SUBSECTION
.Ss Functions
Function descriptions follow:
.Bl -ohang -offset "XXXX"
.\" LIST-ITEM
.It Fn man_alloc
Allocates a parsing structure.  The
.Fa data
pointer is passed to callbacks in
.Fa cb ,
which are documented further in the header file.
The
.Fa pflags
arguments are defined in
.Pa man.h .
Returns NULL on failure.  If non-NULL, the pointer must be freed with
.Fn man_free .
.\" LIST-ITEM
.It Fn man_reset
Reset the parser for another parse routine.  After its use,
.Fn man_parseln
behaves as if invoked for the first time.
.\" LIST-ITEM
.It Fn man_free
Free all resources of a parser.  The pointer is no longer valid after
invocation.
.\" LIST-ITEM
.It Fn man_parseln
Parse a nil-terminated line of input.  This line should not contain the
trailing newline.  Returns 0 on failure, 1 on success.  The input buffer
.Fa buf
is modified by this function.
.\" LIST-ITEM
.It Fn man_endparse
Signals that the parse is complete.  Note that if
.Fn man_endparse
is called subsequent to
.Fn man_node ,
the resulting tree is incomplete.  Returns 0 on failure, 1 on success.
.\" LIST-ITEM
.It Fn man_node
Returns the first node of the parse.  Note that if
.Fn man_parseln
or
.Fn man_endparse
return 0, the tree will be incomplete.
.It Fn man_meta
Returns the document's parsed meta-data.  If this information has not
yet been supplied or
.Fn man_parseln
or
.Fn man_endparse
return 0, the data will be incomplete.
.El
.\" SUBSECTION
.Ss Variables
The following variables are also defined:
.Bl -ohang -offset "XXXX"
.\" LIST-ITEM
.It Va man_macronames
An array of string-ified token names.
.El
.\" SUBSECTION
.Ss Abstract Syntax Tree
The
.Nm
functions produce an abstract syntax tree (AST) describing input in a
regular form.  It may be reviewed at any time with
.Fn man_nodes ;
however, if called before
.Fn man_endparse ,
or after
.Fn man_endparse
or
.Fn man_parseln
fail, it may be incomplete.
.\" PARAGRAPH
.Pp
This AST is governed by the ontological
rules dictated in
.Xr man 7
and derives its terminology accordingly.
.\" PARAGRAPH
.Pp
The AST is composed of
.Vt struct man_node
nodes with element, root and text types as declared
by the
.Va type
field.  Each node also provides its parse point (the
.Va line ,
.Va sec ,
and
.Va pos
fields), its position in the tree (the
.Va parent ,
.Va child ,
.Va next
and
.Va prev
fields) and some type-specific data.
.\" PARAGRAPH
.Pp
The tree itself is arranged according to the following normal form,
where capitalised non-terminals represent nodes.
.Pp
.Bl -tag -width "ELEMENTXX" -compact -offset "XXXX"
.\" LIST-ITEM
.It ROOT
\(<- mnode+
.It mnode
\(<- ELEMENT | TEXT | BLOCK
.It BLOCK
\(<- HEAD BODY
.It HEAD
\(<- mnode*
.It BODY
\(<- mnode*
.It ELEMENT
\(<- ELEMENT | TEXT*
.It TEXT
\(<- [[:alpha:]]*
.El
.\" PARAGRAPH
.Pp
The only elements capable of nesting other elements are those with
next-lint scope as documented in
.Xr man 7 .
.\" SECTION
.Sh EXAMPLES
The following example reads lines from stdin and parses them, operating
on the finished parse tree with
.Fn parsed .
Note that, if the last line of the file isn't newline-terminated, this
will truncate the file's last character (see
.Xr fgetln 3 ) .
Further, this example does not error-check nor free memory upon failure.
.Bd -literal -offset "XXXX"
struct man *man;
struct man_node *node;
char *buf;
size_t len;
int line;

line = 1;
man = man_alloc(NULL, 0, NULL);

while ((buf = fgetln(fp, &len))) {
	buf[len - 1] = '\\0';
	if ( ! man_parseln(man, line, buf))
		errx(1, "man_parseln");
	line++;
}

if ( ! man_endparse(man))
	errx(1, "man_endparse");
if (NULL == (node = man_node(man)))
	errx(1, "man_node");

parsed(man, node);
man_free(man);
.Ed
.\" SECTION
.Sh SEE ALSO
.Xr mandoc 1 ,
.Xr man 7
.\" SECTION
.Sh AUTHORS
The
.Nm
utility was written by
.An Kristaps Dzonsons Aq kristaps@kth.se .
