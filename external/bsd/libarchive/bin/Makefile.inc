# $NetBSD: Makefile.inc,v 1.1 2008/07/27 19:31:03 joerg Exp $

.include "../Makefile.inc"

BINDIR=		/bin

.if (${MKDYNAMICROOT} == "no")
LDSTATIC?=	-static
.endif
