#	$NetBSD: Makefile.inc,v 1.1 2009/12/19 05:52:01 thorpej Exp $

.include <bsd.own.mk>

LIBDWARF_DIR=	${NETBSDSRCDIR}/external/bsd/libdwarf/dist

CPPFLAGS+=	-I${LIBDWARF_DIR}

WARNS?=		4

.PATH:		${LIBDWARF_DIR}
