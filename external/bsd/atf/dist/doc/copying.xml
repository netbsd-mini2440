<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD Simplified DocBook XML V1.1//EN"
                  "http://docbook.org/xml/simple/1.1/sdocbook.dtd">

<article>

<articleinfo>
  <title>Redistribution terms</title>

  <author>
    <firstname>Julio</firstname>
    <surname>Merino</surname>
    <affiliation>
      <orgname>The NetBSD Foundation</orgname>
    </affiliation>
  </author>
</articleinfo>

<section id="license">

<title>License</title>

<para>Copyright (c) 2007, 2008, 2009 The NetBSD Foundation, Inc.  All
rights reserved.</para>

<para>Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:</para>

<orderedlist>

<listitem><para>Redistributions of source code must retain the above
copyright notice, this list of conditions and the following
disclaimer.</para></listitem>

<listitem><para>Redistributions in binary form must reproduce the above
copyright notice, this list of conditions and the following disclaimer in
the documentation and/or other materials provided with the
distribution.</para></listitem>

</orderedlist>

<para>THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND
CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</para>

</section>

<section>

<title>Relicensed code</title>

<para>The following code snippets have been taken from other projects.
Even though they were not originally licensed under the terms above, the
original authors have agreed to relicense their work so that this project
can be distributed under a single license.  This section is put here just
to clarify this fact.</para>

<itemizedlist>

<listitem>
  <para><filename>configure.ac</filename>,
  <filename>Makefile.am</filename>: The original versions were derived from
  the ones in the XML Catalog Manager project, version 2.2.</para>

  <para>Author: Julio M. Merino Vidal
  <email>jmmv@users.sourceforge.net</email></para>
</listitem>

<listitem>
  <para><filename>atf-c/ui.c</filename>: The format_paragraph and
  format_text functions were derived form the ones in the Monotone project,
  revision 3a0982da308228d796df35f98d787c5cff2bb5b6.</para>

  <para>Author: Julio M. Merino Vidal <email>jmmv@NetBSD.org</email></para>
</listitem>

<listitem>
  <para><filename>atf-c++/io.hpp</filename>,
  <filename>atf-c++/io.cpp</filename>,
  <filename>tests/atf-c++/t_io.cpp</filename>: These files were derived
  from the file_handle, systembuf, pipe, pistream and postream classes and
  tests found in the Boost.Process library.</para>

  <para>Author: Julio M. Merino Vidal
  <email>jmmv84@gmail.com</email></para>
</listitem>

<listitem>
  <para><filename>admin/check-style.sh</filename>,
  <filename>admin/check-style-common.awk</filename>,
  <filename>admin/check-style-cpp.awk</filename>,
  <filename>admin/check-style-shell.awk</filename>: These files, except the
  first one, were first implemented in the Buildtool project.  They were
  later adapted to be part of Boost.Process and, during that process, the
  shell script was created.</para>

  <para>Author: Julio M. Merino Vidal
  <email>jmmv84@gmail.com</email></para>
</listitem>

</itemizedlist>

</section>

</article>
