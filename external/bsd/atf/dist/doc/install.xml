<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD Simplified DocBook XML V1.1//EN"
                  "http://docbook.org/xml/simple/1.1/sdocbook.dtd">

<article>

<articleinfo>
  <title>Installation instructions</title>

  <author>
    <firstname>Julio</firstname>
    <surname>Merino</surname>
    <affiliation>
      <orgname>The NetBSD Foundation</orgname>
    </affiliation>
  </author>
</articleinfo>

<section id="introduction">

<title>Introduction</title>

<para>ATF uses the GNU Automake, GNU Autoconf and GNU Libtool utilities as
its build system.  These are used only when compiling the application from
the source code package.  If you want to install ATF from a binary package,
you do not need to read this document.</para>

<para>For the impatient:</para>

<programlisting role="screen">
$ <userinput>./configure</userinput>
$ <userinput>make</userinput>
Gain root privileges
# <userinput>make install</userinput>
Drop root privileges
$ <userinput>make installcheck</userinput>
</programlisting>

<para>Or alternatively, install as a regular user into your home
directory:</para>

<programlisting role="screen">
$ <userinput>./configure --prefix ~/local</userinput>
$ <userinput>make</userinput>
$ <userinput>make install</userinput>
$ <userinput>make installcheck</userinput>
</programlisting>

</section>

<section>

<title>Dependencies</title>

<para>To build and use ATF successfully you need:</para>

<itemizedlist>

<listitem><para>A standards-compliant C/C++ complier.  For example, GNU GCC
2.95 will not work.</para></listitem>

<listitem><para>A POSIX shell interpreter.</para></listitem>

<listitem><para>A make(1) utility.</para></listitem>

</itemizedlist>

<para>If you are building ATF from the code on the repository, you will also need
the following tools.  The versions listed here are the ones used to build
the files bundled in the last formal release, but these are not strictly
required.  Newer ones will most likely work and, maybe, some slightly older
ones:</para>

<itemizedlist>

<listitem><para>GNU autoconf 2.65</para></listitem>

<listitem><para>GNU automake 1.11.1</para></listitem>

<listitem><para>GNU libtool 2.2.6b</para></listitem>

</itemizedlist>

<para>If you are building the XML documentation (which is a requisite to be able
to generate a distfile), you will also need the following tools:</para>

<itemizedlist>

<listitem><para>links</para></listitem>

<listitem><para>The Simple DocBook DTD 1.1</para></listitem>

<listitem><para>tidy</para></listitem>

<listitem><para>xsltproc</para></listitem>

<listitem><para>xmlcatalog and xmllint</para></listitem>

</itemizedlist>

</section>

<section>

<title>Regenerating the build system</title>

<para>If you are building ATF from code extracted from the repository, you must
first regenerate the files used by the build system.  You will also need to
do this if you modify one of configure.ac or Makefile.am.m4.  To do this,
simply run:</para>

<programlisting role="screen">
$ <userinput>./autogen.sh</userinput>
</programlisting>

<para>For formal releases, no extra steps are needed.</para>

</section>

<section>

<title>General build procedure</title>

<para>To build and install the source package, you must follow these
steps:</para>

<orderedlist>

<listitem><para>Configure the sources to adapt to your operating system.
This is done using the 'configure' script located on the sources' top
directory, and it is usually invoked without arguments unless you want to
change the installation prefix.  More details on this procedure are given
on a later section.</para></listitem>

<listitem><para>Build the sources to generate the binaries and scripts.
Simply run 'make' on the sources' top directory after configuring them.  No
problems should arise.</para></listitem>

<listitem><para>Install the program by running 'make install'.  You may
need to become root to issue this step.</para></listitem>

<listitem><para>Issue any manual installation steps that may be required.
These are described later in their own section.</para></listitem>

<listitem><para>Check that the installed programs work by running 'make
installcheck'.  You do not need to be root to do this, even though some
checks will not be run otherwise.  (Be aware that on, some systems, GNU
Libtool will break these checks.  If you get some failures, try
reconfiguring the project providing the '--disable-fast-install' flag to
'configure' and then rebuild and recheck.)</para></listitem>

</orderedlist>

</section>

<section>

<title>Configuration flags</title>

<para>The most common, standard flags given to 'configure' are:</para>

<variablelist>

<varlistentry>
  <term>--prefix=directory</term>

  <listitem>
    <para>Possible values: Any path</para>

    <para>Default: /usr/local</para>

    <para>Specifies where the program (binaries and all associated files)
    will be installed.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>--sysconfdir=directory</term>

  <listitem>
    <para>Possible values: Any path</para>

    <para>Default: /usr/local/etc</para>

    <para>Specifies where the installed programs will look for
    configuration files.  '/atf' will be appended to the given path unless
    ATF_CONFSUBDIR is redefined as explained later on.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>--help</term>

  <listitem>
    <para>Shows information about all available flags and exits
    immediately, without running any configuration tasks.</para>
  </listitem>
</varlistentry>

</variablelist>

<para>The following environment variables are specific to ATF's 'configure'
script:</para>

<variablelist>

<varlistentry>
  <term>ATF_BUILD_CC</term>

  <listitem>
    <para>Possible values: empty, a absolute or relative path to a C
    compiler.</para>

    <para>Default: the value of CC as detected by the configure
    script.</para>

    <para>Specifies the C compiler that ATF will use at run time whenever
    the build-time-specific checks are used.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>ATF_BUILD_CFLAGS</term>

  <listitem>
    <para>Possible values: empty, a list of valid C compiler flags.</para>

    <para>Default: the value of CFLAGS as detected by the configure
    script.</para>

    <para>Specifies the C compiler flags that ATF will use at run time
    whenever the build-time-specific checks are used.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>ATF_BUILD_CPP</term>

  <listitem>
    <para>Possible values: empty, a absolute or relative path to a C/C++
    preprocessor.</para> <para>Default: the value of CPP as detected by the
    configure script.</para>

    <para>Specifies the C/C++ preprocessor that ATF will use at run time
    whenever the build-time-specific checks are used.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>ATF_BUILD_CPPFLAGS</term>

  <listitem>
    <para>Possible values: empty, a list of valid C/C++ preprocessor
    flags.</para>

    <para>Default: the value of CPPFLAGS as detected by the configure
    script.</para>

    <para>Specifies the C/C++ preprocessor flags that ATF will use at run
    time whenever the build-time-specific checks are used.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>ATF_BUILD_CXX</term>

  <listitem>
    <para>Possible values: empty, a absolute or relative path to a C++
    compiler.</para>

    <para>Default: the value of CXX as detected by the configure
    script.</para>

    <para>Specifies the C++ compiler that ATF will use at run time whenever
    the build-time-specific checks are used.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>ATF_BUILD_CXXFLAGS</term>

  <listitem>
    <para>Possible values: empty, a list of valid C++ compiler
    flags.</para>

    <para>Default: the value of CXXFLAGS as detected by the configure
    script.</para>

    <para>Specifies the C++ compiler flags that ATF will use at run time
    whenever the build-time-specific checks are used.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>ATF_CONFSUBDIR</term>

  <listitem>
    <para>Possible values: empty, a relative path.</para>

    <para>Default: atf.</para>

    <para>Specifies the subdirectory of the configuration directory (given
    by the --sysconfdir argument) under which ATF will search for its
    configuration files.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>ATF_M4</term>

  <listitem>
    <para>Possible values: empty, absolute path to a M4 macro
    processor.</para>

    <para>Default: empty.</para>

    <para>Specifies the M4 macro processor that ATF will use at run time to
    generate GNU Automake files.  If empty, the configure script will try
    to find a suitable M4 implementation for you.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>ATF_SHELL</term>

  <listitem>
    <para>Possible values: empty, absolute path to a POSIX shell
    interpreter.</para>

    <para>Default: empty.</para>

    <para>Specifies the POSIX shell interpreter that ATF will use at run
    time to execute its scripts and the test programs written using the
    atf-sh library.  If empty, the configure script will try to find a
    suitable interpreter for you.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>ATF_WORKDIR</term>

  <listitem>
    <para>Possible values: empty, an absolute path.</para>

    <para>Default: /tmp or /var/tmp, depending on availability.</para>

    <para>Specifies the directory that ATF will use to place its temporary
    files and work directories for test cases.  This is just a default and
    can be overriden at run time.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>LINKS</term>

  <listitem>
    <para>Possible values: absolute path.</para>

    <para>Default: links.</para>

    <para>Specifies the program name or the absolute path of the 'links'
    application.  Required when --enable-doc-build is used; otherwise
    ignored.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>TIDY</term>

  <listitem>
    <para>Possible values: absolute path.</para>

    <para>Default: tidy.</para>

    <para>Specifies the program name or the absolute path of the 'tidy'
    tool.  Required when --enable-doc-build is used; otherwise
    ignored.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>XMLCATALOG</term>

  <listitem>
    <para>Possible values: absolute path.</para>

    <para>Default: xmlcatalog.</para>

    <para>Specifies the program name or the absolute path of the
    'xmlcatalog' tool.  Required when --enable-doc-build is used; otherwise
    ignored.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>XML_CATALOG_FILE</term>

  <listitem>
    <para>Possible values: absolute path.</para>

    <para>Default: /etc/xml/catalog</para>

    <para>Specifies the path to the system-wide XML catalog used to lookup
    the required DTDs.  The build will NOT perform any network access to
    load them.  Required when --enable-doc-build is used; otherwise
    ignored.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>XMLLINT</term>

  <listitem>
    <para>Possible values: absolute path.</para>

    <para>Default: xmllint.</para>

    <para>Specifies the program name or the absolute path of the 'xmllint'
    tool.  Required when --enable-doc-build is used; otherwise
    ignored.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>XSLTPROC</term>

  <listitem>
    <para>Possible values: absolute path.</para>

    <para>Default: xsltproc.</para>

    <para>Specifies the program name or the absolute path of the 'xsltproc'
    tool.  Required when --enable-doc-build is used; otherwise
    ignored.</para>
  </listitem>
</varlistentry>

</variablelist>

<para>The following flags are specific to ATF's 'configure' script:</para>

<variablelist>

<varlistentry>
  <term>--enable-developer</term>
  <listitem>
    <para>Possible values: yes, no</para>

    <para>Default: Depends on the version number.  Stable versions define
    this to 'no' while all others have it set to 'yes'.</para>

    <para>Enables several features useful for development, such as the
    inclusion of debugging symbols in all objects or the enabling of
    warnings during compilation.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>--enable-doc-build</term>
  <listitem>
    <para>Possible values: yes, no</para>

    <para>Default: no.</para>

    <para>Enables the building of the XML documentation.  This must be
    enabled in order to be able to generate a distribution file (aka run
    'make dist').  Disabled by default because the toolchain required to
    enable this feature is pretty big, and not everyone needs it: the
    distribution files come with up-to-date, pregenerated
    documentation.</para>
  </listitem>
</varlistentry>

<varlistentry>
  <term>--enable-unstable-shared</term>
  <listitem>
    <para>Possible values: yes, no</para>

    <para>Default: no.</para>

    <para>Forces the building of shared libraries in addition to static
    ones.  The build of shared libraries is currently disabled because
    their ABIs and APIs are unstable and subject to change.  This flag is
    provided for development purposes only and will be removed once the
    libraries are stable enough.</para>
  </listitem>
</varlistentry>

</variablelist>

</section>

<section>

<title>Post-installation steps</title>

<para>After installing ATF, you have to register the DTDs it provides into
the system-wide XML catalog.  See the comments at the top of the files in
${datadir}/share/xml/atf to see the correct public identifiers.  This
directory will typically be /usr/local/share/xml/atf or /usr/share/xml/atf.
Failure to do so will lead to further errors when processing the XML files
generated by atf-report.</para>

</section>

</article>
