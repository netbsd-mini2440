.\"
.\" Automated Testing Framework (atf)
.\"
.\" Copyright (c) 2008, 2009 The NetBSD Foundation, Inc.
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND
.\" CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
.\" INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
.\" MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
.\" IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS BE LIABLE FOR ANY
.\" DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
.\" GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
.\" INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
.\" IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
.\" OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
.\" IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
.\"
.Dd July 17, 2009
.Dt ATF-C-API 3
.Os
.Sh NAME
.Nm ATF_CHECK ,
.Nm ATF_CHECK_MSG ,
.Nm ATF_CHECK_EQ ,
.Nm ATF_CHECK_EQ_MSG ,
.Nm ATF_CHECK_STREQ ,
.Nm ATF_CHECK_STREQ_MSG ,
.Nm ATF_REQUIRE ,
.Nm ATF_REQUIRE_MSG ,
.Nm ATF_REQUIRE_EQ ,
.Nm ATF_REQUIRE_EQ_MSG ,
.Nm ATF_REQUIRE_STREQ ,
.Nm ATF_REQUIRE_STREQ_MSG ,
.Nm ATF_TC ,
.Nm ATF_TC_BODY ,
.Nm ATF_TC_BODY_NAME ,
.Nm ATF_TC_CLEANUP ,
.Nm ATF_TC_CLEANUP_NAME ,
.Nm ATF_TC_HEAD ,
.Nm ATF_TC_HEAD_NAME ,
.Nm ATF_TC_NAME ,
.Nm ATF_TC_WITH_CLEANUP ,
.Nm ATF_TP_ADD_TC ,
.Nm ATF_TP_ADD_TCS ,
.Nm atf_no_error ,
.Nm atf_tc_fail ,
.Nm atf_tc_fail_nonfatal ,
.Nm atf_tc_pass ,
.Nm atf_tc_skip
.Nd C API to write ATF-based test programs
.Sh SYNOPSIS
.In atf-c.h
.Fn ATF_CHECK "expression"
.Fn ATF_CHECK_MSG "expression" "fail_msg_fmt" ...
.Fn ATF_CHECK_EQ "expression_1" "expression_2"
.Fn ATF_CHECK_EQ_MSG "expression_1" "expression_2" "fail_msg_fmt" ...
.Fn ATF_CHECK_STREQ "string_1" "string_2"
.Fn ATF_CHECK_STREQ_MSG "string_1" "string_2" "fail_msg_fmt" ...
.Fn ATF_REQUIRE "expression"
.Fn ATF_REQUIRE_MSG "expression" "fail_msg_fmt" ...
.Fn ATF_REQUIRE_EQ "expression_1" "expression_2"
.Fn ATF_REQUIRE_EQ_MSG "expression_1" "expression_2" "fail_msg_fmt" ...
.Fn ATF_REQUIRE_STREQ "string_1" "string_2"
.Fn ATF_REQUIRE_STREQ_MSG "string_1" "string_2" "fail_msg_fmt" ...
.Fn ATF_TC "name"
.Fn ATF_TC_BODY "name" "tc"
.Fn ATF_TC_BODY_NAME "name"
.Fn ATF_TC_CLEANUP "name" "tc"
.Fn ATF_TC_CLEANUP_NAME "name"
.Fn ATF_TC_HEAD "name" "tc"
.Fn ATF_TC_HEAD_NAME "name"
.Fn ATF_TC_NAME "name"
.Fn ATF_TC_WITH_CLEANUP "name"
.Fn ATF_TP_ADD_TC "tp_name"
.Fn ATF_TP_ADD_TCS "tp_name" "tc_name"
.Fn atf_no_error
.Fn atf_tc_fail "reason"
.Fn atf_no_error
.Fn atf_tc_fail_nonfatal "reason"
.Fn atf_tc_pass
.Fn atf_tc_skip "reason"
.Sh DESCRIPTION
The ATF
.Pp
C-based test programs always follow this template:
.Bd -literal -offset indent
.Ns ... C-specific includes go here ...

#include <atf-c.h>

ATF_TC(tc1);
ATF_TC_HEAD(tc1, tc)
{
    ... first test case's header ...
}
ATF_TC_BODY(tc1, tc)
{
    ... first test case's body ...
}

ATF_TC_WITH_CLEANUP(tc2);
ATF_TC_HEAD(tc2, tc)
{
    ... second test case's header ...
}
ATF_TC_BODY(tc2, tc)
{
    ... second test case's body ...
}
ATF_TC_CLEANUP(tc2, tc)
{
    ... second test case's cleanup ...
}

.Ns ... additional test cases ...

ATF_TP_ADD_TCS(tp, tcs)
{
    ATF_TP_ADD_TC(tcs, tc1)
    ATF_TP_ADD_TC(tcs, tc2)
    ... add additional test cases ...

    return atf_no_error();
}
.Ed
.Ss Definition of test cases
Test cases have an identifier and are composed of three different parts:
the header, the body and an optional cleanup routine, all of which are
described in
.Xr atf-test-case 8 .
To define test cases, one can use the
.Fn ATF_TC
or the
.Fn ATF_TC_WITH_CLEANUP
macros, which take a single parameter specifiying the test case's name.
The former does not allow the specification of a cleanup routine for the
test case while the latter does.
It is important to note that these
.Em do not
set the test case up for execution when the program is run.
In order to do so, a later registration is needed with the
.Fn ATF_TP_ADD_TC
macro detailed in
.Sx Program initialization .
.Pp
Later on, one must define the three parts of the body by means of three
functions.
Their headers are given by the
.Fn ATF_TC_HEAD ,
.Fn ATF_TC_BODY
and
.Fn ATF_TC_CLEANUP
macros, all of which take the test case name provided to the
.Fn ATF_TC
or
.Fn ATF_TC_WITH_CLEANUP
macros and the name of the variable that will hold a pointer to the
test case data.
Following each of these, a block of code is expected, surrounded by the
opening and closing brackets.
.Ss Program initialization
The library provides a way to easily define the test program's
.Fn main
function.
You should never define one on your own, but rely on the
library to do it for you.
This is done by using the
.Fn ATF_TP_ADD_TCS
macro, which is passed the name of the object that will hold the test
cases; i.e. the test program instance.
This name can be whatever you want as long as it is a valid variable
identifier.
.Pp
After the macro, you are supposed to provide the body of a function, which
should only use the
.Fn ATF_TP_ADD_TC
macro to register the test cases the test program will execute and return
a success error code.
The first parameter of this macro matches the name you provided in the
former call.
The success status can be returned using the
.Fn atf_no_error
function.
.Ss Header definitions
The test case's header can define the meta-data by using the
.Fn atf_tc_set_md_var
method, which takes three parameters: the first one points to the test
case data, the second one specifies the meta-data variable to be set
and the third one specifies its value.
Both of them are strings.
.Ss Configuration variables
The test case has read-only access to the current configuration variables
by means of the
.Ft bool
.Fn atf_tc_has_config_var
and the
.Ft const char *
.Fn atf_tc_get_config_var
methods, which can be called in any of the three parts of a test case.
.Ss Access to the source directory
It is possible to get the path to the test case's source directory from any
of its three components by querying the
.Sq srcdir
configuration variable.
.Ss Requiring programs
Aside from the
.Va require.progs
meta-data variable available in the header only, one can also check for
additional programs in the test case's body by using the
.Fn atf_tc_require_prog
function, which takes the base name or full path of a single binary.
Relative paths are forbidden.
If it is not found, the test case will be automatically skipped.
.Ss Test case finalization
The test case finalizes either when the body reaches its end, at which
point the test is assumed to have
.Em passed ,
unless any non-fatal errors were raised using
.Fn atf_tc_fail_nonfatal ,
or at any explicit call to
.Fn atf_tc_pass ,
.Fn atf_tc_fail
or
.Fn atf_tc_skip .
These three functions terminate the execution of the test case immediately.
The cleanup routine will be processed afterwards in a completely automated
way, regardless of the test case's termination reason.
.Pp
.Fn atf_tc_pass
does not take any parameters.
.Fn atf_tc_fail ,
.Fn atf_tc_fail_nonfatal
and
.Fn atf_tc_skip
take a format string and a variable list of parameters, which describe, in
a user-friendly manner, why the test case failed or was skipped,
respectively.
It is very important to provide a clear error message in both cases so that
the user can quickly know why the test did not pass.
.Ss Helper macros for common checks
The library provides several macros that are very handy in multiple
situations.
These basically check some condition after executing a given statement or
processing a given expression and, if the condition is not met, they
report the test case as failed.
.Pp
The
.Sq REQUIRE
variant of the macros immediately abort the test case as soon as an error
condition is detected by calling the
.Fn atf_tc_fail
function.
Use this variant whenever it makes now sense to continue the execution of a
test case when the checked condition is not met.
The
.Sq CHECK
variant, on the other hand, reports a failure as soon as it is encountered
using the
.Fn atf_tc_fail_nonfatal
function, but the execution of the test case continues as if nothing had
happened.
Use this variant whenever the checked condition is important as a result of
the test case, but there are other conditions that can be subsequently
checked on the same run without aborting.
.Pp
Additionally, the
.Sq MSG
variants take an extra set of parameters to explicitly specify the failure
message.
This failure message is formatted according to the
.Xr printf 3
formatters.
.Pp
.Fn ATF_CHECK ,
.Fn ATF_CHECK_MSG ,
.Fn ATF_REQUIRE
and
.Fn ATF_REQUIRE_MSG
take an expression and fail if the expression evaluates to false.
.Pp
.Fn ATF_CHECK_EQ ,
.Fn ATF_CHECK_EQ_MSG ,
.Fn ATF_REQUIRE_EQ
and
.Fn ATF_REQUIRE_EQ_MSG
take two expressions and fail if the two evaluated values are not equal.
.Pp
.Fn ATF_CHECK_STREQ ,
.Fn ATF_CHECK_STREQ_MSG ,
.Fn ATF_REQUIRE_STREQ
and
.Fn ATF_REQUIRE_STREQ_MSG
take two strings and fail if the two are not equal character by character.
.Sh EXAMPLES
The following shows a complete test program with a single test case that
validates the addition operator:
.Bd -literal -offset indent
#include <atf-c.h>

ATF_TC(addition);
ATF_TC_HEAD(addition, tc)
{
    atf_tc_set_md_var(tc, "descr",
                      "Sample tests for the addition operator");
}
ATF_TC_BODY(addition, tc)
{
    ATF_CHECK_EQ(0 + 0, 0);
    ATF_CHECK_EQ(0 + 1, 1);
    ATF_CHECK_EQ(1 + 0, 1);

    ATF_CHECK_EQ(1 + 1, 2);

    ATF_CHECK_EQ(100 + 200, 300);
}

ATF_TC(string_formatting);
ATF_TC_HEAD(string_formatting, tc)
{
    atf_tc_set_md_var(tc, "descr",
                      "Sample tests for the snprintf");
}
ATF_TC_BODY(string_formatting, tc)
{
    char buf[1024];
    snprintf(buf, sizeof(buf), "a %s", "string");
    ATF_CHECK_STREQ_MSG("a string", buf, "%s is not working");
}

ATF_TP_ADD_TCS(tp)
{
    ATF_TP_ADD_TC(tp, addition);
    ATF_TP_ADD_TC(tp, string_formatting);

    return atf_no_error();
}
.Ed
.Sh SEE ALSO
.Xr atf-test-program 1 ,
.Xr atf 7 ,
.Xr atf-test-case 8
