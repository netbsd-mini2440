/* $NetBSD: nbcompat.h,v 1.3 2008/02/07 23:46:47 joerg Exp $ */

/*
 * In pkgsrc, libnbcompat is used to provide compatibility functions.
 * This is not needed on native NetBSD, so provide an empty header.
 */

#include <sys/statvfs.h>
