#	$NetBSD$

NOMAN=		# defined
WARNS=		3

CPPFLAGS+=      -I${.CURDIR}/..

regress:
	@if ./${PROG}; then		\
		echo PASSED; exit 0;	\
	else				\
		echo FAILED; exit 1;	\
	fi
