#	$NetBSD: Makefile.inc,v 1.1 2006/04/22 02:19:47 uwe Exp $
#
#	etc.mmeye/Makefile.inc -- mmeye-specific etc Makefile targets
#

# If you change the list of distributed kernels, don't forget
# to update the release documentation in distrib/notes/common/contents

KERNEL_SETS=		GENERIC
