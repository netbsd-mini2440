#	$NetBSD: Makefile.inc,v 1.4 2005/05/18 14:04:26 chs Exp $
#
#	etc.hp700/Makefile.inc -- hp700-specific etc Makefile targets
#

# If you change the list of distributed kernels, don't forget
# to update the release documentation in distrib/notes/common/contents

KERNEL_SETS=		GENERIC

BUILD_KERNELS=		RAMDISK

INSTALLATION_DIRS+=     installation
