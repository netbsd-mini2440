#	$NetBSD: Makefile.inc,v 1.3 2007/07/24 10:49:24 pavel Exp $
#
#	etc.cobalt/Makefile.inc -- cobalt-specific etc Makefile targets
#

# If you change the list of distributed kernels, don't forget
# to update the release documentation in distrib/notes/common/contents

KERNEL_SETS=		GENERIC INSTALL

BUILD_KERNELS=		RAMDISK
