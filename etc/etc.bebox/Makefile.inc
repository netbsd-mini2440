#	$NetBSD: Makefile.inc,v 1.4 2002/12/02 13:30:59 lukem Exp $
#
#	etc.bebox/Makefile.inc -- bebox-specific etc Makefile targets
#

# If you change the list of distributed kernels, don't forget
# to update the release documentation in distrib/notes/common/contents

KERNEL_SETS=		GENERIC

BUILD_KERNELS=		INSTALL
