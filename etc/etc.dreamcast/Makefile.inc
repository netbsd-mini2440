#	$NetBSD: Makefile.inc,v 1.2 2002/12/06 09:09:05 jdolecek Exp $
#
#	etc.dreamcast/Makefile.inc -- dreamcast-specific etc Makefile targets
#

# If you change the list of distributed kernels, don't forget
# to update the release documentation in distrib/notes/common/contents

KERNEL_SETS=		GENERIC

BUILD_KERNELS=		GENERIC_MD
