#	$NetBSD: Makefile.inc,v 1.1 2004/10/13 23:28:35 gavan Exp $
#
#	etc.iyonix/Makefile.inc -- iyonix-specific etc Makefile targets
#

# If you change the list of distributed kernels, don't forget
# to update the release documentation in distrib/notes/common/contents

KERNEL_SETS=		GENERIC
