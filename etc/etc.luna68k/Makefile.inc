#	$NetBSD: Makefile.inc,v 1.3 2002/12/02 13:18:08 lukem Exp $
#
#	etc.luna68k/Makefile.inc -- luna68k-specific etc Makefile targets
#

# If you change the list of distributed kernels, don't forget
# to update the release documentation in distrib/notes/common/contents

KERNEL_SETS=		GENERIC

INSTALLATION_DIRS+=	installation/netboot
